# 中国云OS最小内核

#### 介绍
中国云操作系统最小内核开源项目，基于中国云标准API。中国云OS最小内核定位于打造成一个标准的最小化核心的基础云平台，功能组件包括计算、存储、网络、认证。

![技术架构](https://images.gitee.com/uploads/images/2019/1025/113844_a545a04d_4915185.png "temp_001.png")
 **最小内核分三层** 
1. 第三方调用及应用接口层：用于统一的第三方及应用调用，实现应用于底层云厂家的解耦，应用只需要关注与业务与云计算资源的交互，无需关心云计算资源来源于哪朵云
2. 服务层管理层及API：用于多云的统一资源管理，实现多云的多租户、资源配额、多云管理等管理功能
3. 虚拟化层API：用户多云之间的数据迁移及共享

云OS生态建设遵循“开放APIS+开源内核，可演化、可扩展、可服务、可标准化”。
![应用生态](https://images.gitee.com/uploads/images/2019/1025/135850_598cf9dd_4915185.png "temp_004.png")

 **最小内核应用意义** 
- 多云互联互通：实现云间互联互通，又能凸现不同云的差异化服务
- 云上应用解耦：上层用户与应用的高可移植性，基于中国云API开发，可在不同云之间无缝迁移
- 大数据云服务：促进大数据与云计算的融合发展
- 统一云标准与生态：形成具备多层次一致标准的应用生态，促进云计算行业的持续迭代创新
 **最小内核生态标准化** 
- 目标：
    以构建面向世界的、自主可控云 OS 应用生态为出发点，突破多项云计算关键技术；研制基于软件定义的新一代云计算最小内核，通过阿里、华为、国云、中国电科在政务、电商等领域推广应用，最终形成云 OS 的 API 标准规范及应用生态体系。
- 指导原则：
    1. 以基础API为开源核心，实现最小云平台
    2. 构建开放生态，开设不同特色项目板块
    3. 鼓励基于核心API，开发符合国云、阿里云、华为、OPENSTACK等多云组件
 
**主要功能模块** 
- 弹性计算服务：
1. 地域管理
2. 虚拟机管理
3. 磁盘管理
4. 镜像管理
5. 快照管理
6. 专有网络管理
7. 外部网络管理
8. 路由管理
9. 交换机管理
10. 安全组客理
11. 网卡管理
12. 弹性公网IP管理

- 负载均衡服务：
1. 负载均衡实例管理
2. 监听器管理
3. 后端服务管理
4. 调度策略管理
5. 健康检查


#### 软件架构
1. 后端：
- 基础框架：Spring Boot 1.5.13.RELEASE
- 持久层框架：gcloud-framework-db-7.1.0
- 安全框架：gcloud-service-identity-8.0.0
- 数据库连接池：阿里巴巴Druid 1.0.26
- 消息队列：rabbitmq
- 缓存框架：redis
- 日志打印：log4j
- 其他：fastjson，quartz, lombok（简化代码）等

2. 开发环境：
- 语言：Java 8
- IDE(JAVA)： Eclipse安装lombok插件 或者 IDEA
- 依赖管理：Maven
- 数据库：Mariadb
- 缓存：Redis


#### 安装教程

1. [详见本项目附件“云操作系统最新内核编译部署V1.0.docx”](https://gitee.com/osgcloud/mini-kernel/attach_files/250645/download)

#### 使用说明

 **后台开发环境和依赖** 

1. java
2. maven
3. jdk8
4. mariadb
5. 数据库脚步：gcloud\gcloud-boot\sql\gcloud_controller_quartz.sql gcloud\gcloud-boot\sql\gcloud_controller.sql
6. 默认登录账号： admin/gcloud123 （源码版本免登录）


 **项目下载和运行** 
1. 拉取项目代码
直接下载本项目，或者远程拉取git clone http://113.105.131.176:29780/gcloud/g-cloud-8.0.git
cd g-cloud-8.0/gcloud
2. 编译
mvn clean;mvn package;
3. 运行
cd  gcloud/gcloud-boot/target/
nohup java -jar gcloud-boot-8.0.jar &
4. 接口测试（以获取vpc列表为例）
curl -i -X POST -H "X-Auth-Token: 4723ad1b-5190-3fc0-8c1b-96581dda3841" http://127.0.0.1:8089/ecs/DescribeVpcs.do
