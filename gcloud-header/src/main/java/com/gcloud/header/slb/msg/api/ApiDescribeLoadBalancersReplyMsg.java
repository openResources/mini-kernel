package com.gcloud.header.slb.msg.api;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.slb.model.DescribeLoadBalancersResponse;
import com.gcloud.header.slb.model.LoadBalancerModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDescribeLoadBalancersReplyMsg extends PageReplyMessage<LoadBalancerModel> {
	
	private static final long serialVersionUID = 1L;

	@ApiModel(description = "负载均衡信息")
	private DescribeLoadBalancersResponse loadBalancers;
	
	@Override
	public void setList(List<LoadBalancerModel> list) {
		// TODO Auto-generated method stub
		loadBalancers = new DescribeLoadBalancersResponse();
	    loadBalancers.setLoadBalancers(list);
	}

	public DescribeLoadBalancersResponse getLoadBalancers() {
		return loadBalancers;
	}

	public void setLoadBalancers(DescribeLoadBalancersResponse loadBalancers) {
		this.loadBalancers = loadBalancers;
	}
	
}