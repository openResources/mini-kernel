package com.gcloud.header.image.msg.api.iso;

import com.gcloud.header.ApiPageMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDescribeIsosMsg extends ApiPageMessage{
	private static final long serialVersionUID = 1L;

    @Override
    public Class replyClazz() {
        return ApiDescribeIsosReplyMsg.class;
    }

    @ApiModel(description = "映像 ID", require = false)
    private String isoId;
    
    @ApiModel(description = "映像名称", require = false)
    private String isoName;
    
    @ApiModel(description = "映像状�?�，saving:保存�?, active:可用", require = false)
    private String status;
    
    @ApiModel(description = "是否禁用。true:禁用, false:可用", require = false)
    private Boolean disable;
    
    @ApiModel(description = "映像类型，system:系统光盘, other:其他", require = false)
    private String isoType;
    
	public String getIsoId() {
		return isoId;
	}

	public void setIsoId(String isoId) {
		this.isoId = isoId;
	}

	public String getIsoName() {
		return isoName;
	}

	public void setIsoName(String isoName) {
		this.isoName = isoName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getDisable() {
		return disable;
	}

	public void setDisable(Boolean disable) {
		this.disable = disable;
	}

	public String getIsoType() {
		return isoType;
	}

	public void setIsoType(String isoType) {
		this.isoType = isoType;
	}
}