package com.gcloud.header.image.msg.api.iso;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiImportIsoMsg extends ApiMessage{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@NotBlank(message = "0090501::映像名称不能为空")
	@Length(max = 255, message = "0090502::名称长度不能大于255")
    @ApiModel(description = "映像名称", require=true)
    private String isoName;
    @NotBlank(message = "0090503::映像路径不能为空")
    @ApiModel(description = "映像文件路径", require=true)
    private String filePath;
    @NotBlank(message = "0090504::映像类型不能为空")
    @ApiModel(description = "映像类型，system:系统光盘, other:其他", require=true)
    private String isoType;//system\other
    @ApiModel(description = "系统类型，linux、windows")
    private String osType;
    @ApiModel(description = "架构, x86_64、i386�? i686")
    private String architecture;
    @ApiModel(description = "系统版本")
    private String osVersion;//如CentOS 7.0 Minimal-1503�?
    @ApiModel(description = "备注")
    private String description;
    
	@Override
	public Class replyClazz() {
		return ApiImportIsoReplyMsg.class;
	}

	public String getIsoName() {
		return isoName;
	}


	public void setIsoName(String isoName) {
		this.isoName = isoName;
	}


	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getIsoType() {
		return isoType;
	}

	public void setIsoType(String isoType) {
		this.isoType = isoType;
	}

	public String getOsType() {
		return osType;
	}

	public void setOsType(String osType) {
		this.osType = osType;
	}

	public String getArchitecture() {
		return architecture;
	}

	public void setArchitecture(String architecture) {
		this.architecture = architecture;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}