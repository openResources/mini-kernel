package com.gcloud.header.compute.msg.api.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DetailInstanceType implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "实例详情ID")
	private String id;
	@ApiModel(description = "实例详情名称")
	private String name;
	@JsonProperty(value = "CpuCoreCount")
	@ApiModel(description = "CPU核数")
	private Integer vcpus;
	@JsonProperty(value = "MemorySize")
	@ApiModel(description = "内存大小，单位MB")
	private Double memoryMb; // 单位MB
	@ApiModel(description = "可用区列�?")
	private List<ZoneInfo> availableZones;
	@ApiModel(description = "是否可用�? true:可用, false:禁用")
	private boolean enabled;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getVcpus() {
		return vcpus;
	}
	public void setVcpus(Integer vcpus) {
		this.vcpus = vcpus;
	}
	public Double getMemoryMb() {
		return memoryMb;
	}
	public void setMemoryMb(Double memoryMb) {
		this.memoryMb = memoryMb;
	}
	public List<ZoneInfo> getAvailableZones() {
		return availableZones;
	}
	public void setAvailableZones(List<ZoneInfo> availableZones) {
		this.availableZones = availableZones;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}