package com.gcloud.header.compute.msg.api.vm.base.standard;

import java.util.List;

import com.gcloud.header.ApiPageMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardApiDescribeInstancesMsg extends ApiPageMessage{
	private static final long serialVersionUID = 1L;

	@Override
	public Class replyClazz() {
		return StandardApiDescribeInstancesReplyMsg.class;
	}
	
	@ApiModel(description="实例名称")
	private String instanceName;
	@ApiModel(description="实例状�??")
	private String status;
	@ApiModel(description="可用区ID")
	private String zoneId;
	@ApiModel(description="实例ID列表")
	private List<String> instanceIds;
	//云盘挂载时，搜索虚拟机列表时�?
	@ApiModel(description="挂载云盘ID")
	private String attachDiskId;

	public String getInstanceName() {
		return instanceName;
	}
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getZoneId() {
		return zoneId;
	}
	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}
	public List<String> getInstanceIds() {
		return instanceIds;
	}
	public void setInstanceIds(List<String> instanceIds) {
		this.instanceIds = instanceIds;
	}
	public String getAttachDiskId() {
		return attachDiskId;
	}
	public void setAttachDiskId(String attachDiskId) {
		this.attachDiskId = attachDiskId;
	}
}