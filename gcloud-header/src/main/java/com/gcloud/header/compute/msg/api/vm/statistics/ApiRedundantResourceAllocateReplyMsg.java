package com.gcloud.header.compute.msg.api.vm.statistics;

import java.util.List;

import com.gcloud.header.ListReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.compute.msg.api.model.RedundantResourceAllocateItem;
import com.gcloud.header.compute.msg.api.model.RedundantResourceAllocateResponse;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiRedundantResourceAllocateReplyMsg extends ListReplyMessage<RedundantResourceAllocateItem>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "统计�?")
	RedundantResourceAllocateResponse allocateItems;

	public RedundantResourceAllocateResponse getAllocateItems() {
		return allocateItems;
	}

	public void setAllocateItems(RedundantResourceAllocateResponse allocateItems) {
		this.allocateItems = allocateItems;
	}

	@Override
	public void setList(List<RedundantResourceAllocateItem> list) {
		allocateItems = new RedundantResourceAllocateResponse();
		allocateItems.setAllocateItem(list);
	}
}