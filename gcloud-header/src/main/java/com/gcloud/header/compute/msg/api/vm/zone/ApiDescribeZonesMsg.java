package com.gcloud.header.compute.msg.api.vm.zone;

import com.gcloud.header.ApiPageMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class ApiDescribeZonesMsg extends ApiPageMessage {

    private static final long serialVersionUID = 1L;
    
    @ApiModel(description = "是否可用。true:可用, false:禁用", require = false)
    private Boolean enabled;

    @ApiModel(description = "存储池ID。一般是本地存储类型和集中存储类�?")
    private String poolId;

    @ApiModel(description = "磁盘类型")
    private String diskCategoryId;

    @ApiModel(description = "是否只显示简单信�?")
    private Boolean simple;

    @Override
    public Class replyClazz() {
        return ApiDescribeZonesReplyMsg.class;
    }

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getPoolId() {
		return poolId;
	}

	public void setPoolId(String poolId) {
		this.poolId = poolId;
	}

    public String getDiskCategoryId() {
        return diskCategoryId;
    }

    public void setDiskCategoryId(String diskCategoryId) {
        this.diskCategoryId = diskCategoryId;
    }

    public Boolean getSimple() {
        return simple;
    }

    public void setSimple(Boolean simple) {
        this.simple = simple;
    }
}