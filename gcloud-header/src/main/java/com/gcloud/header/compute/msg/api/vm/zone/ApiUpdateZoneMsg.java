package com.gcloud.header.compute.msg.api.vm.zone;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiUpdateZoneMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;

	@ApiModel(description = "可用区ID", require = true)
	@NotBlank(message = "::可用区ID不能为空")
	private String zoneId;
	
	@ApiModel(description = "可用区名�?", require = true)
	@NotBlank(message = "::可用区名称不能为�?")
	private String zoneName;
	
	@Override
	public Class replyClazz() {
		return ApiUpdateZoneReplyMsg.class;
	}

	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}
}