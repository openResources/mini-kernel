package com.gcloud.header.identity.role;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDetailRoleMsg extends ApiMessage{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModel(description = "角色ID", require = true)
	@NotBlank(message="2030401::角色ID不能为空")
	private String roleId;
	
	@ApiModel(description = "区域ID")
	private String paramRegionId;//与RegionFilter中的regionId区分

	@Override
	public Class replyClazz() {
		return ApiDetailRoleReplyMsg.class;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getParamRegionId() {
		return paramRegionId;
	}

	public void setParamRegionId(String paramRegionId) {
		this.paramRegionId = paramRegionId;
	}


}