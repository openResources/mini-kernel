package com.gcloud.header.identity.role;

import java.util.Date;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.identity.role.model.RoleFunctionRightResponse;
import com.gcloud.header.identity.role.model.RoleResourceRightResponse;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDetailRoleReplyMsg extends ApiReplyMessage{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description="角色ID")
	private String id;
	@ApiModel(description="角色名称")
	private String name;
	@ApiModel(description="创建�?")
	private String creator;
	@ApiModel(description="创建时间")
	private Date createTime;
	@ApiModel(description="角色描述")
	private String description;
	@ApiModel(description="角色类型")
	private String roleType;
	@ApiModel(description="资源权限")
	RoleResourceRightResponse roleResourceRightItems;
	@ApiModel(description="功能权限")
	RoleFunctionRightResponse roleFunctionRightItems;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRoleType() {
		return roleType;
	}
	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}
	public RoleResourceRightResponse getRoleResourceRightItems() {
		return roleResourceRightItems;
	}
	public void setRoleResourceRightItems(RoleResourceRightResponse roleResourceRightItems) {
		this.roleResourceRightItems = roleResourceRightItems;
	}
	public RoleFunctionRightResponse getRoleFunctionRightItems() {
		return roleFunctionRightItems;
	}
	public void setRoleFunctionRightItems(RoleFunctionRightResponse roleFunctionRightItems) {
		this.roleFunctionRightItems = roleFunctionRightItems;
	}
}