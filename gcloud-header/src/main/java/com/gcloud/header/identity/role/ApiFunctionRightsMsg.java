package com.gcloud.header.identity.role;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiFunctionRightsMsg extends ApiMessage{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description="是否是公共功�?")
	private boolean commonFunc;

	@Override
	public Class replyClazz() {
		return ApiFunctionRightsReplyMsg.class;
	}

	public boolean isCommonFunc() {
		return commonFunc;
	}

	public void setCommonFunc(boolean commonFunc) {
		this.commonFunc = commonFunc;
	}

}