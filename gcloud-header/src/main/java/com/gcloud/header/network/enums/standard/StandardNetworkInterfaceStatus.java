package com.gcloud.header.network.enums.standard;

import com.gcloud.header.network.enums.PortStatus;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public enum StandardNetworkInterfaceStatus {

    AVAILABLE("Available", "可使�?", Arrays.asList(PortStatus.ACTIVE, PortStatus.DOWN)),
    IN_USE("InUse", "已使�?", null),
    INITING("Initing", "初始化中", Arrays.asList(PortStatus.BUILD, PortStatus.PENDING_CREATE, PortStatus.PENDING_UPDATE, PortStatus.PENDING_DELETE)),
    ERROR("Error", "错误", Arrays.asList(PortStatus.ERROR)),
    DELETED("Deleted", "已删�?", null);

    StandardNetworkInterfaceStatus(String value, String cnName, List<PortStatus> gcStatus) {
        this.value = value;
        this.cnName = cnName;
        this.gcStatus = gcStatus;
    }

    private String value;
    private String cnName;
    private List<PortStatus> gcStatus;

    public static String standardStatus(String gcStatusStr){

        if(StringUtils.isBlank(gcStatusStr)){
            return gcStatusStr;
        }

        for(StandardNetworkInterfaceStatus status : StandardNetworkInterfaceStatus.values()){
            if(status.gcStatus == null || status.getGcStatus().size() == 0){
                continue;
            }
            for(PortStatus gcStatus : status.getGcStatus()){
                if(gcStatus.value().equals(gcStatusStr)){
                    return status.getValue();
                }
            }
        }

        return null;
    }

    public String getValue() {
        return value;
    }

    public String getCnName() {
        return cnName;
    }

    public List<PortStatus> getGcStatus() {
        return gcStatus;
    }
}