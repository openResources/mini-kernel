package com.gcloud.header.network.msg.api.standard;


import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardApiUnassociateEipAddressMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@NotBlank(message = "0050301::弹�?�公网实例Id不能为空")
	@ApiModel(description = "弹�?�公网实例Id", require = true)
	private String allocationId;
	@ApiModel(description = "实例Id")
	@NotBlank(message = "0050303::实例id不能为空")
	private String instanceId;
	@NotBlank(message = "0050304::实例类型不能为空")
	@ApiModel(description = "实例类型", require = true)
	private String instanceType;//Netcard

	@Override
	public Class replyClazz() {
		return StandardApiUnassociateEipAddressReplyMsg.class;
	}

	public String getAllocationId() {
		return allocationId;
	}

	public void setAllocationId(String allocationId) {
		this.allocationId = allocationId;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getInstanceType() {
		return instanceType;
	}

	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}
}