package com.gcloud.header.network.msg.api;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.controller.ControllerProperty;
import com.gcloud.header.network.model.DescribeSecurityGroupResponse;
import com.gcloud.header.network.model.SecurityGroupItemType;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class DescribeSecurityGroupsReplyMsg extends PageReplyMessage<SecurityGroupItemType>{
	@ApiModel(description="安全组列�?")
	private DescribeSecurityGroupResponse securityGroups;
	@ApiModel(description="区域ID")
	private String regionId = ControllerProperty.REGION_ID;

	@Override
	public void setList(List<SecurityGroupItemType> list) {
		securityGroups = new DescribeSecurityGroupResponse();
		securityGroups.setSecurityGroup(list);
		
	}
	public DescribeSecurityGroupResponse getSecurityGroups() {
		return securityGroups;
	}
	public void setSecurityGroups(DescribeSecurityGroupResponse securityGroups) {
		this.securityGroups = securityGroups;
	}

	public String getRegionId() {
		return regionId;
	}

	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
}