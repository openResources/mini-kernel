package com.gcloud.header.network.msg.api;


import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class CreateVRouterMsg extends ApiMessage {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModel(description="路由器名�?",require=true)
	@NotBlank(message="0020101")
	private String vRouterName;

	public String getvRouterName() {
		return vRouterName;
	}

	public void setvRouterName(String vRouterName) {
		this.vRouterName = vRouterName;
	}
	@Override
	public Class replyClazz() {
		return CreateVRouterReplyMsg.class;
	}

}