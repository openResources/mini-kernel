package com.gcloud.header.network.msg.api.standard;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiIgnore;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.network.model.standard.StandardDescribeVpcsResponse;
import com.gcloud.header.network.model.standard.StandardVpcType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardApiDescribeVpcsReplyMsg extends PageReplyMessage<StandardVpcType>{
	private static final long serialVersionUID = 1L;

	@ApiModel(description = "虚拟私有云列�?")
	private StandardDescribeVpcsResponse vpcs;
	
	@Override
	public void setList(List<StandardVpcType> list) {
		vpcs = new StandardDescribeVpcsResponse();
		vpcs.setVpc(list);
	}

	public StandardDescribeVpcsResponse getVpcs() {
		return vpcs;
	}

	public void setVpcs(StandardDescribeVpcsResponse vpcs) {
		this.vpcs = vpcs;
	}
}