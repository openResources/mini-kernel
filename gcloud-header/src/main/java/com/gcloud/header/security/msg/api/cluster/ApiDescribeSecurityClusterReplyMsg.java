package com.gcloud.header.security.msg.api.cluster;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.security.model.DescribeSecurityClusterResponse;
import com.gcloud.header.security.model.SecurityClusterType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDescribeSecurityClusterReplyMsg extends PageReplyMessage<SecurityClusterType> {
	
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "安全集群列表")
	private DescribeSecurityClusterResponse securityClusters;

    @Override
    public void setList(List<SecurityClusterType> list) {
    	securityClusters = new DescribeSecurityClusterResponse();
    	securityClusters.setSecurityCluster(list);
    }

	public DescribeSecurityClusterResponse getSecurityClusters() {
		return securityClusters;
	}

	public void setSecurityClusters(DescribeSecurityClusterResponse securityClusters) {
		this.securityClusters = securityClusters;
	}
}