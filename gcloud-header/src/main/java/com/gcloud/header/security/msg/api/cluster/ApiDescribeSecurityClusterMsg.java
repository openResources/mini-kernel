package com.gcloud.header.security.msg.api.cluster;

import com.gcloud.header.ApiPageMessage;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDescribeSecurityClusterMsg extends ApiPageMessage {

	private static final long serialVersionUID = 1L;
	
	@Override
    public Class replyClazz() {
        return ApiDescribeSecurityClusterReplyMsg.class;
    }
    
	@ApiModel(description = "安全集群名称")
	private String securityClusterName;

	public String getSecurityClusterName() {
		return securityClusterName;
	}

	public void setSecurityClusterName(String securityClusterName) {
		this.securityClusterName = securityClusterName;
	}
}