package com.gcloud.header.storage.msg.api.snapshot.standard;

import java.util.List;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.storage.model.standard.StandardDescribeSnapshotsResponse;
import com.gcloud.header.storage.model.standard.StandardSnapshotType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class StandardApiDescribeSnapshotsReplyMsg extends PageReplyMessage<StandardSnapshotType>{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "快照列表")
	private StandardDescribeSnapshotsResponse snapshots;

	@Override
	public void setList(List<StandardSnapshotType> list) {
		snapshots = new StandardDescribeSnapshotsResponse();
		snapshots.setSnapshot(list);
	}

	public StandardDescribeSnapshotsResponse getSnapshots() {
		return snapshots;
	}

	public void setSnapshots(StandardDescribeSnapshotsResponse snapshots) {
		this.snapshots = snapshots;
	}
}