package com.gcloud.header.storage.msg.api.volume;

import com.gcloud.header.PageReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.storage.model.DiskItemType;
import com.gcloud.header.storage.model.DescribeDisksResponse;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class ApiDescribeDisksReplyMsg extends PageReplyMessage<DiskItemType> {

    private static final long serialVersionUID = 1L;

    @ApiModel(description = "磁盘列表")
    private DescribeDisksResponse disks;

    @Override
    public void setList(List<DiskItemType> list) {
        disks = new DescribeDisksResponse();
        disks.setDisk(list);
    }

    public DescribeDisksResponse getDisks() {
        return disks;
    }

    public void setDisks(DescribeDisksResponse disks) {
        this.disks = disks;
    }
}