package com.gcloud.header.storage.msg.api.pool;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;

import com.gcloud.header.ApiMessage;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.storage.StorageErrorCodes;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiAssociateDiskCategoryMorePoolMsg extends ApiMessage{
	private static final long serialVersionUID = 1L;
	
	@ApiModel(description = "磁盘类型ID", require = true)
	@NotBlank(message = StorageErrorCodes.INPUT_DISK_CATEGORY_ERROR)
	private String diskCategoryId;
	
	@ApiModel(description = "可用区ID", require = true)
	@NotBlank(message = StorageErrorCodes.INPUT_ZONE_ID_ERROR)
	private String zoneId;
	
	@ApiModel(description = "存储池ID列表", require = true)
	private List<String> poolIds = new ArrayList<>();

	@Override
	public Class replyClazz() {
		return ApiReplyMessage.class;
	}

	public String getDiskCategoryId() {
		return diskCategoryId;
	}

	public void setDiskCategoryId(String diskCategoryId) {
		this.diskCategoryId = diskCategoryId;
	}

	public String getZoneId() {
		return zoneId;
	}

	public void setZoneId(String zoneId) {
		this.zoneId = zoneId;
	}

	public List<String> getPoolIds() {
		return poolIds;
	}

	public void setPoolIds(List<String> poolIds) {
		this.poolIds = poolIds;
	}
}