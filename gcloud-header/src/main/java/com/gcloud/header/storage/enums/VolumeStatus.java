package com.gcloud.header.storage.enums;

import java.util.Arrays;

import com.google.common.base.CaseFormat;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public enum VolumeStatus {

    AVAILABLE("可用"),
    ATTACHING("挂载�?"),
    CREATING_BACKUP("备份�?"),
    CREATING("创建�?"),
    DELETING("删除�?"),
    DOWNLOADING("下载�?"),
    UPLOADING("上传�?"),
    ERROR("错误"),
    ERROR_DELETING("删除错误"),
    ERROR_RESTORING("还原错误"),
    IN_USE("已用"),
    RESTORING_BACKUP("备份还原�?"),
    DETACHING("卸载�?"),
    UNRECOGNIZED("未知"),
    RESIZING("扩展�?");

    private String cnName;

    VolumeStatus(String cnName) {
        this.cnName = cnName;
    }

    public String value() {
        return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
    }

    public String getCnName() {
        return cnName;
    }
    
    public static String getCnName(String value) {
    	VolumeStatus enu =  Arrays.stream(VolumeStatus.values()).filter(type -> type.value().equals(value)).findFirst().orElse(null);
		return enu != null ? enu.getCnName() : null;
	}
}