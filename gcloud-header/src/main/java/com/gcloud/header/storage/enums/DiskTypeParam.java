package com.gcloud.header.storage.enums;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public enum DiskTypeParam {

    ALL("all", "�?�?"), SYSTEM(DiskType.SYSTEM.getValue(), "系统�?"), DATA(DiskType.DATA.getValue(), "数据�?");

    private String value;
    private String cnName;


    DiskTypeParam(String value, String cnName) {
        this.value = value;
        this.cnName = cnName;
    }

    public String getValue() {
        return value;
    }

    public String getCnName() {
        return cnName;
    }

    public static DiskTypeParam getByValue(String value){

        DiskTypeParam result = null;
        if(value != null){
            for(DiskTypeParam param : DiskTypeParam.values()){
                if(value.equals(param.getValue())){
                    result = param;
                    break;
                }
            }
        }
        return result;
    }

}