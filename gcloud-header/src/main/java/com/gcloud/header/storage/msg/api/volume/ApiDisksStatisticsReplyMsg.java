package com.gcloud.header.storage.msg.api.volume;

import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.api.ApiModel;
import com.gcloud.header.storage.model.DiskCategoryStatisticsResponse;
import com.gcloud.header.storage.model.DiskStatusStatisticsResponse;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class ApiDisksStatisticsReplyMsg extends ApiReplyMessage{
	private static final long serialVersionUID = 1L;
	@ApiModel(description = "总数�?")
	private int allNum;
	@ApiModel(description = "磁盘状�?�统计信�?")
	private DiskStatusStatisticsResponse statusStatisticsItems;
	@ApiModel(description = "磁盘类型统计信息")
	private DiskCategoryStatisticsResponse categoryStatisticsItems;
	public int getAllNum() {
		return allNum;
	}
	public void setAllNum(int allNum) {
		this.allNum = allNum;
	}
	public DiskStatusStatisticsResponse getStatusStatisticsItems() {
		return statusStatisticsItems;
	}
	public void setStatusStatisticsItems(DiskStatusStatisticsResponse statusStatisticsItems) {
		this.statusStatisticsItems = statusStatisticsItems;
	}
	public DiskCategoryStatisticsResponse getCategoryStatisticsItems() {
		return categoryStatisticsItems;
	}
	public void setCategoryStatisticsItems(DiskCategoryStatisticsResponse categoryStatisticsItems) {
		this.categoryStatisticsItems = categoryStatisticsItems;
	}
	
}