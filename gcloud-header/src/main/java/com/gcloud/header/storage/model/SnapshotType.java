package com.gcloud.header.storage.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcloud.framework.db.jdbc.annotation.TableField;
import com.gcloud.header.api.ApiModel;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class SnapshotType implements Serializable {

    private static final long serialVersionUID = 1L;
    @ApiModel(description="快照ID")
    @TableField("id")
    private String snapshotId;
    @ApiModel(description="快照名称")
    @TableField("display_name")
    private String snapshotName;
    @ApiModel(description="描述")
    @TableField("display_description")
    private String description;
    @ApiModel(description="云盘ID")
    @TableField("volume_id")
    private String sourceDiskId;
    @ApiModel(description="云盘名称")
    private String sourceDiskName;
    @ApiModel(description="创建时间")
    @TableField("created_at")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date creationTime;
    @ApiModel(description="快照状�??")
    private String status;
    @ApiModel(description="中文快照状�??")
    private String cnStatus;
    @ApiModel(description="云盘大小")
    @TableField("volume_size")
    private String sourceDiskSize;


	public String getSourceDiskName() {
		return sourceDiskName;
	}

	public void setSourceDiskName(String sourceDiskName) {
		this.sourceDiskName = sourceDiskName;
	}

	public String getCnStatus() {
		return cnStatus;
	}

	public void setCnStatus(String cnStatus) {
		this.cnStatus = cnStatus;
	}

	public String getSnapshotId() {
        return snapshotId;
    }

    public void setSnapshotId(String snapshotId) {
        this.snapshotId = snapshotId;
    }

    public String getSnapshotName() {
        return snapshotName;
    }

    public void setSnapshotName(String snapshotName) {
        this.snapshotName = snapshotName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSourceDiskId() {
        return sourceDiskId;
    }

    public void setSourceDiskId(String sourceDiskId) {
        this.sourceDiskId = sourceDiskId;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSourceDiskSize() {
        return sourceDiskSize;
    }

    public void setSourceDiskSize(String sourceDiskSize) {
        this.sourceDiskSize = sourceDiskSize;
    }
}