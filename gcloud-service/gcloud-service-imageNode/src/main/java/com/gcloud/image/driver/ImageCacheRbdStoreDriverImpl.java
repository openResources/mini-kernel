package com.gcloud.image.driver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gcloud.common.util.StringUtils;
import com.gcloud.common.util.SystemUtil;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.image.prop.ImageNodeProp;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
public class ImageCacheRbdStoreDriverImpl implements IImageCacheStoreDriver {
	@Autowired
	ImageNodeProp imageNodeProp;

	@Override
	public void deleteImageCache(String imageId, String storeTarget, String resourceType) {
		String poolName = storeTarget;
		String[] snapLsCmd = new String[] {"rbd", "--user", imageNodeProp.getCephOperator(), "snap", "ls", poolName + "/" + imageId};
    	if(StringUtils.isNotBlank(SystemUtil.run(snapLsCmd))) {
        	String[] snapUnProtectCmd = new String[] {"rbd", "--user", imageNodeProp.getCephOperator(), "snap", "unprotect", poolName + "/" + imageId +"@snap"};
        	int unProtectRes = SystemUtil.runAndGetCode(snapUnProtectCmd);
        	if(unProtectRes != 0) {
            	throw new GCloudException(String.format("::%s池镜�?%s快照解保护失�?", storeTarget, imageId));
            }
        	
        	String[] snapDelCmd = new String[] {"rbd", "--user", imageNodeProp.getCephOperator(), "snap", "rm", poolName + "/" + imageId +"@snap"};
        	int snapDelRes = SystemUtil.runAndGetCode(snapDelCmd);
        	if(snapDelRes != 0) {
            	throw new GCloudException(String.format("::%s池镜�?%s快照删除失败", storeTarget, imageId));
            }
    	}
		
		//删掉images池上对应的image cache , rbd rm  -p images  imageId 
		String[] deleteCmd = new String[]{"rbd", "--user", imageNodeProp.getCephOperator(), "rm", "-p", storeTarget , imageId};
        int deleteRes = SystemUtil.runAndGetCode(deleteCmd);
        if(deleteRes != 0) {
        	throw new GCloudException(String.format("::删除%s池上的镜�?%s失败",storeTarget, imageId));
        }
	}

}