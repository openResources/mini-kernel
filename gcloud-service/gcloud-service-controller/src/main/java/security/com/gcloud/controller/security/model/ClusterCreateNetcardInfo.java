package com.gcloud.controller.security.model;

import com.gcloud.controller.network.enums.PortType;
import com.gcloud.controller.security.enums.SecurityNetcardConfigType;
import com.gcloud.controller.security.enums.SecurityNetworkType;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class ClusterCreateNetcardInfo {

    private String subnetId;
    private String securityGroupId;
    private PortType portType;
    private SecurityNetcardConfigType configType;
    private String clusterOvsId;
    private SecurityNetworkType securityNetworkType;

    public String getSubnetId() {
        return subnetId;
    }

    public void setSubnetId(String subnetId) {
        this.subnetId = subnetId;
    }

    public PortType getPortType() {
        return portType;
    }

    public void setPortType(PortType portType) {
        this.portType = portType;
    }

    public SecurityNetcardConfigType getConfigType() {
        return configType;
    }

    public void setConfigType(SecurityNetcardConfigType configType) {
        this.configType = configType;
    }

    public String getClusterOvsId() {
        return clusterOvsId;
    }

    public void setClusterOvsId(String clusterOvsId) {
        this.clusterOvsId = clusterOvsId;
    }

    public String getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(String securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    public SecurityNetworkType getSecurityNetworkType() {
        return securityNetworkType;
    }

    public void setSecurityNetworkType(SecurityNetworkType securityNetworkType) {
        this.securityNetworkType = securityNetworkType;
    }
}