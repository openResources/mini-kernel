package com.gcloud.controller.security.handler.api.cluster;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.security.model.SecurityClusterRemoveInstanceParams;
import com.gcloud.controller.security.service.ISecurityClusterService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.security.msg.api.cluster.ApiSecurityClusterRemoveInstanceMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module = Module.ECS, subModule= SubModule.SECURITYCLUSTER, action = "SecurityClusterRemoveInstance", name = "从安全集群移除云服务�?")
public class ApiSecurityClusterRemoveInstanceHandler extends MessageHandler<ApiSecurityClusterRemoveInstanceMsg, ApiReplyMessage> {

	@Autowired
	private ISecurityClusterService securityClusterService;
	
    @Override
    public ApiReplyMessage handle(ApiSecurityClusterRemoveInstanceMsg msg) throws GCloudException {
    	SecurityClusterRemoveInstanceParams params = BeanUtil.copyProperties(msg, SecurityClusterRemoveInstanceParams.class);
    	securityClusterService.securityClusterRemoveInstance(params, msg.getCurrentUser());
    	
    	ApiReplyMessage reply = new ApiReplyMessage();
        return reply;
    }
}