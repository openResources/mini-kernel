package com.gcloud.controller.security.model;

import com.gcloud.controller.security.enums.SecurityComponent;

import java.util.List;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class ClusterCreateNetInfo {

    private Map<SecurityComponent, List<ClusterCreateNetcardInfo>> createNetcardInfo;
    private List<ClusterCreateOvsBridgeInfo> createOvsBridgeInfos;

    public Map<SecurityComponent, List<ClusterCreateNetcardInfo>> getCreateNetcardInfo() {
        return createNetcardInfo;
    }

    public void setCreateNetcardInfo(Map<SecurityComponent, List<ClusterCreateNetcardInfo>> createNetcardInfo) {
        this.createNetcardInfo = createNetcardInfo;
    }

    public List<ClusterCreateOvsBridgeInfo> getCreateOvsBridgeInfos() {
        return createOvsBridgeInfos;
    }

    public void setCreateOvsBridgeInfos(List<ClusterCreateOvsBridgeInfo> createOvsBridgeInfos) {
        this.createOvsBridgeInfos = createOvsBridgeInfos;
    }
}