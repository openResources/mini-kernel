package com.gcloud.controller.security.enums;


import com.google.common.base.CaseFormat;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public enum SecurityComponent {
    FIREWALL("防火�?", true, true),
    WAF("waf", true, true),
    ISMS("isms", true, true),
    FORTRESS("堡垒�?", false, false);

    private String cnName;
    private boolean hasHa;
    private boolean zxAuth;

    SecurityComponent(String cnName, boolean hasHa, boolean zxAuth) {
        this.cnName = cnName;
        this.hasHa = hasHa;
        this.zxAuth = zxAuth;
    }

    public static SecurityComponent getByValue(String value){
        return Arrays.stream(SecurityComponent.values()).filter(c -> c.value().equals(value)).findFirst().orElse(null);
    }

    public static Map<String, String> valueCnMap(){
        Map<String, String> result = new HashMap<>();
        Arrays.stream(SecurityComponent.values()).forEach(c -> result.put(c.value(), c.getCnName()));
        return result;
    }

    public String value() {
        return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_HYPHEN, name());
    }

    public String getCnName() {
        return cnName;
    }

    public boolean getHasHa() {
        return hasHa;
    }

    public boolean getZxAuth() {
        return zxAuth;
    }
}