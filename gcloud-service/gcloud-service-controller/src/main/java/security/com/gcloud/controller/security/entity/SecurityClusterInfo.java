package com.gcloud.controller.security.entity;


import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Table(name = "gc_security_cluster_info")
public class SecurityClusterInfo {

    @ID
    private Integer id;
    private String clusterId;
    private String hostname;
    private Boolean ha;
    
    
    public static final String ID = "id";
    public static final String CLUSTER_ID = "clusterId";
    public static final String HOST_NAME = "hostname";
    public static final String HA = "ha";

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClusterId() {
        return clusterId;
    }

    public void setClusterId(String clusterId) {
        this.clusterId = clusterId;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public Boolean getHa() {
        return ha;
    }

    public void setHa(Boolean ha) {
        this.ha = ha;
    }
    
    public String updateId(Integer id) {
    	this.setId(id);
    	return ID;
    }
    
    public String updateClusterId(String clusterId) {
    	this.setClusterId(clusterId);
    	return CLUSTER_ID;
    }
    
    public String updateHostname(String hostname) {
    	this.setHostname(hostname);
    	return HOST_NAME;
    }
    
    public String updateHa(Boolean ha) {
    	this.setHa(ha);
    	return HA;
    }
}