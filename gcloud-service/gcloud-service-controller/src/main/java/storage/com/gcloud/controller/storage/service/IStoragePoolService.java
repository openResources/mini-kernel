package com.gcloud.controller.storage.service;

import com.gcloud.controller.storage.entity.DiskCategory;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.model.AssociateDiskCategoryMorePoolParams;
import com.gcloud.controller.storage.model.AssociateDiskCategoryPoolParams;
import com.gcloud.controller.storage.model.AssociateDiskCategoryZoneParams;
import com.gcloud.controller.storage.model.AssociatePoolZoneParams;
import com.gcloud.controller.storage.model.CreateDiskCategoryParams;
import com.gcloud.controller.storage.model.DeleteDiskCategoryParams;
import com.gcloud.controller.storage.model.DescribeDiskCategoriesParams;
import com.gcloud.controller.storage.model.DescribeStoragePoolsParams;
import com.gcloud.controller.storage.model.DetailDiskCategoryParams;
import com.gcloud.controller.storage.model.DetailPoolParams;
import com.gcloud.controller.storage.model.EnableDiskCategoryParams;
import com.gcloud.controller.storage.model.ModifyDiskCategoryParams;
import com.gcloud.controller.storage.model.UnassociateDiskCategoryPoolParams;
import com.gcloud.controller.storage.model.node.ReportStoragePoolParams;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.storage.model.DiskCategoryModel;
import com.gcloud.header.storage.model.DiskCategoryType;
import com.gcloud.header.storage.model.StoragePoolModel;
import com.gcloud.header.storage.model.StoragePoolType;
import com.gcloud.header.storage.model.StorageTypeVo;
import com.gcloud.header.storage.msg.api.pool.ApiPoolsStatisticsReplyMsg;
import com.gcloud.header.storage.msg.node.pool.LocalPoolReportMsg;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public interface IStoragePoolService {

    List<DiskCategoryModel> describeDiskCategories(String zoneId);
    
    PageResult<DiskCategoryType> describeDiskCategories(DescribeDiskCategoriesParams params);

    String createDiskCategory(CreateDiskCategoryParams params);

    void modifyDiskCategory(ModifyDiskCategoryParams params, CurrentUser currentUser);
    
    void deleteDiskCategory(DeleteDiskCategoryParams params, CurrentUser currentUser);
    
    DiskCategoryModel detailDiskCategory(DetailDiskCategoryParams params, CurrentUser currentUser);
    
    PageResult<StoragePoolModel> describeStoragePools(int pageNumber, int pageSize, String poolId);
    
    PageResult<StoragePoolModel> describeStoragePools(DescribeStoragePoolsParams params);

    String createStoragePool(String displayName, Integer providerType, String storageType, String poolName, String zoneId, /*String categoryId,*/ String hostname, String driverName,
            String taskId) throws GCloudException;

    String reportStoragePool(String displayName, Integer providerType, String storageType, String poolName, String categoryCode, String hostname, String driverName)
            throws GCloudException;
    
    String reportStoragePool(ReportStoragePoolParams params, String taskId) throws GCloudException;
    
    void modifyStoragePool(String poolId, String displayName) throws GCloudException;

    void deleteStoragePool(String poolId) throws GCloudException;

	void associateDiskCategoryZone(AssociateDiskCategoryZoneParams params);
	
	void associateDiskCategoryPool(AssociateDiskCategoryPoolParams params);
	void associateDiskCategoryMorePool(AssociateDiskCategoryMorePoolParams params);
	
	void unassociateDiskCategoryPool(UnassociateDiskCategoryPoolParams params);
	
	void associatePoolZone(AssociatePoolZoneParams params, CurrentUser currentUser);
	
    ApiPoolsStatisticsReplyMsg poolStatistics(String poolId);
    
    StoragePool getPoolById(String poolId);
    
    void enableDiskCategory(EnableDiskCategoryParams params, CurrentUser currentUser);
    
    StoragePoolType deatilPool(DetailPoolParams params, CurrentUser currentUser);
    
    DiskCategory getDiskCategoryById(String diskCategoryId);

    List<String> categoryHost(String category, String zoneId);

    List<String> categoryHost(DiskCategory category, String zoneId);

    StoragePool assignStoragePool(String category, String zoneId, String createHost);

    StoragePool assignStoragePool(DiskCategory category, String zoneId, String createHost);
    
    void updateLocalPoolZone(String hostName, String zoneId);
    
    void clearLocalPoolZone(String hostName);
    
    void initCollectStorageInfoCron(String batchId);
    
    void collectStorageInfos(String batchId, String[] storageTypes);
    
    void reportLocalPool(LocalPoolReportMsg msg);

    List<StorageTypeVo> diskCategoryStorageType();
    List<StorageTypeVo> storagePoolStorageType();
}