package com.gcloud.controller.storage.provider.impl;

import com.gcloud.controller.provider.CinderProviderProxy;
import com.gcloud.controller.storage.entity.StoragePool;
import com.gcloud.controller.storage.provider.IStoragePoolProvider;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.enums.ResourceType;
import com.gcloud.header.storage.model.StoragePoolInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Component
public class CinderStoragePoolProvider implements IStoragePoolProvider {

    @Autowired
    private CinderProviderProxy proxy;

    @Override
    public ResourceType resourceType() {
        return ResourceType.STORAGE_POOL;
    }

    @Override
    public ProviderType providerType() {
        return ProviderType.CINDER;
    }

    @Override
    public String createStoragePool(String poolId, String storageType, String poolName, String hostname, String taskId) throws GCloudException {
        return this.proxy.createVolumeType(poolName, poolName);
    }

    @Override
    public void deleteStoragePool(String storageType, String providerRefId, String poolName) throws GCloudException {
        this.proxy.deleteVolumeType(providerRefId);
    }

	@Override
	public StoragePoolInfo getPoolSize(StoragePool pool) {
		return this.proxy.getPoolInfo(pool);
	}

}