package com.gcloud.controller.storage.handler.api.snapshot.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.ResourceIsolationCheck;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.controller.storage.service.ISnapshotService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.msg.api.snapshot.standard.StandardApiModifySnapshotAttributeMsg;
import com.gcloud.header.storage.msg.api.snapshot.standard.StandardApiModifySnapshotAttributeReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@GcLog(taskExpect = "修改快照属�??")
@ApiHandler(module= Module.ECS,subModule=SubModule.SNAPSHOT, action="ModifySnapshotAttribute", versions = {ApiVersion.Standard}, name = "修改快照属�??")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.SNAPSHOT, resourceIdField = "snapshotId")
public class StandardApiModifySnapshotAttributeHandler extends MessageHandler<StandardApiModifySnapshotAttributeMsg, StandardApiModifySnapshotAttributeReplyMsg>{

	@Autowired
    private ISnapshotService snapshotService;
	
	@Override
	public StandardApiModifySnapshotAttributeReplyMsg handle(StandardApiModifySnapshotAttributeMsg msg)
			throws GCloudException {
		snapshotService.updateSnapshot(msg.getSnapshotId(), msg.getSnapshotName(), null);
        msg.setObjectId(msg.getSnapshotId());
        msg.setObjectName(CacheContainer.getInstance().getString(CacheType.SNAPSHOT_NAME, msg.getSnapshotId()));
        return new StandardApiModifySnapshotAttributeReplyMsg();
	}

}