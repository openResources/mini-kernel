package com.gcloud.controller.storage.handler.api.snapshot.standard;

import com.gcloud.controller.storage.model.DescribeSnapshotsParams;
import com.gcloud.controller.storage.service.ISnapshotService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.ApiUtil;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.storage.model.SnapshotType;
import com.gcloud.header.storage.model.standard.StandardSnapshotType;
import com.gcloud.header.storage.msg.api.snapshot.standard.StandardApiDescribeSnapshotsMsg;
import com.gcloud.header.storage.msg.api.snapshot.standard.StandardApiDescribeSnapshotsReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module= Module.ECS,subModule=SubModule.SNAPSHOT, action="DescribeSnapshots", versions = {ApiVersion.Standard}, name = "查询快照列表")
public class StandardApiDescribeSnapshotsHandler extends MessageHandler<StandardApiDescribeSnapshotsMsg, StandardApiDescribeSnapshotsReplyMsg>{

	@Autowired
    private ISnapshotService snapshotService;
	
	@Override
	public StandardApiDescribeSnapshotsReplyMsg handle(StandardApiDescribeSnapshotsMsg msg) throws GCloudException {
		DescribeSnapshotsParams params = BeanUtil.copyProperties(msg, DescribeSnapshotsParams.class);
        PageResult<SnapshotType> response = snapshotService.describeSnapshots(params, msg.getCurrentUser());
        PageResult<StandardSnapshotType> stdResponse = ApiUtil.toPage(response, toStandardReply(response.getList()));
        
        StandardApiDescribeSnapshotsReplyMsg reply = new StandardApiDescribeSnapshotsReplyMsg();
        reply.init(stdResponse);
        return reply;
	}
	
	public List<StandardSnapshotType> toStandardReply(List<SnapshotType> list) {
		if(null == list) {
			return null;
		}
		
		List<StandardSnapshotType> stdList = new ArrayList<>();
		for (SnapshotType data : list) {
			StandardSnapshotType stdData = BeanUtil.copyBean(data, StandardSnapshotType.class);
			stdList.add(stdData);
		}
		return stdList;
	}

}