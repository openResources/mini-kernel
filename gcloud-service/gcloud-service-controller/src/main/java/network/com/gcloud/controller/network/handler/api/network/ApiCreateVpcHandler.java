package com.gcloud.controller.network.handler.api.network;


import com.gcloud.controller.network.service.IVpcService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.network.msg.api.CreateVpcMsg;
import com.gcloud.header.network.msg.api.CreateVpcReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@GcLog(taskExpect = "创建虚拟私有云成�?")
@ApiTypeAnno(apiType=ApiTypeConst.CREATE)
@ApiHandler(module=Module.ECS,subModule=SubModule.VPC,action="CreateVpc",name="创建虚拟私有�?")
public class ApiCreateVpcHandler extends MessageHandler<CreateVpcMsg, CreateVpcReplyMsg> {
	
	@Autowired
	IVpcService service;
	
	@Override
	public CreateVpcReplyMsg handle(CreateVpcMsg msg) throws GCloudException {
		// TODO Auto-generated method stub
		CreateVpcReplyMsg reply = new CreateVpcReplyMsg();
		String vpcId = service.createVpc(msg);
		reply.setVpcId(vpcId);
		
		msg.setObjectId(vpcId);
		msg.setObjectName(msg.getVpcName());
		return reply;
	}

}