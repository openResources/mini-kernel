package com.gcloud.controller.network.entity;

import com.gcloud.controller.ResourceProviderEntity;
import com.gcloud.framework.db.jdbc.annotation.ID;
import com.gcloud.framework.db.jdbc.annotation.Table;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Table(name = "gc_qos_bandwidth_limit_rules", jdbc = "controllerJdbcTemplate")
public class QosBandwidthLimitRule extends ResourceProviderEntity {

    @ID
    private String id;
    private String qosPolicyId;
    private Integer maxKbps;
    private Integer maxBurstKbps;
    private String direction;

    public static final String ID = "id";
    public static final String QOS_POLICY_ID = "qosPolicyId";
    public static final String MAX_KBPS = "maxKbps";
    public static final String MAX_BURST_KBPS = "maxBurstKbps";
    public static final String DIRECTION = "direction";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQosPolicyId() {
        return qosPolicyId;
    }

    public void setQosPolicyId(String qosPolicyId) {
        this.qosPolicyId = qosPolicyId;
    }

    public Integer getMaxKbps() {
        return maxKbps;
    }

    public void setMaxKbps(Integer maxKbps) {
        this.maxKbps = maxKbps;
    }

    public Integer getMaxBurstKbps() {
        return maxBurstKbps;
    }

    public void setMaxBurstKbps(Integer maxBurstKbps) {
        this.maxBurstKbps = maxBurstKbps;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }


    public String updateId(String id) {
        this.setId(id);
        return ID;
    }

    public String updateQosPolicyId(String qosPolicyId) {
        this.setQosPolicyId(qosPolicyId);
        return QOS_POLICY_ID;
    }

    public String updateMaxKbps(Integer maxKbps) {
        this.setMaxKbps(maxKbps);
        return MAX_KBPS;
    }

    public String updateMaxBurstKbps(Integer maxBurstKbps) {
        this.setMaxBurstKbps(maxBurstKbps);
        return MAX_BURST_KBPS;
    }

    public String updateDirection(String direction) {
        this.setDirection(direction);
        return DIRECTION;
    }
}