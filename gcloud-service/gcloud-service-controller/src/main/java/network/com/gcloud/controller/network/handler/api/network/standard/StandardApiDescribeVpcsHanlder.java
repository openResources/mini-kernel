package com.gcloud.controller.network.handler.api.network.standard;

import com.gcloud.controller.ResourceIsolationCheck;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.controller.network.service.IVpcService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.ApiUtil;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.model.VpcsItemType;
import com.gcloud.header.network.model.standard.StandardVpcType;
import com.gcloud.header.network.msg.api.DescribeVpcsMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiDescribeVpcsMsg;
import com.gcloud.header.network.msg.api.standard.StandardApiDescribeVpcsReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module=Module.ECS,subModule=SubModule.VPC,action="DescribeVpcs", versions = {ApiVersion.Standard}, name = "虚拟私有云列�?")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.ROUTER, resourceIdField = "vpcIds")
public class StandardApiDescribeVpcsHanlder extends MessageHandler<StandardApiDescribeVpcsMsg, StandardApiDescribeVpcsReplyMsg>{

	@Autowired
	IVpcService service;
	
	@Override
	public StandardApiDescribeVpcsReplyMsg handle(StandardApiDescribeVpcsMsg msg) throws GCloudException {
		PageResult<VpcsItemType> response = service.describeVpcs(toParams(msg));
		PageResult<StandardVpcType> stdResponse = ApiUtil.toPage(response, toStandardReply(response.getList()));
		
		StandardApiDescribeVpcsReplyMsg reply = new StandardApiDescribeVpcsReplyMsg();
        reply.init(stdResponse);
		return reply;
	}
	
	public DescribeVpcsMsg toParams(StandardApiDescribeVpcsMsg msg) {
		DescribeVpcsMsg params = BeanUtil.copyProperties(msg, DescribeVpcsMsg.class);
		return params;
	}
	
	public List<StandardVpcType> toStandardReply(List<VpcsItemType> list) {
		if(null == list) {
			return null;
		}
		
		List<StandardVpcType> stdList = new ArrayList<>();
		
		for (VpcsItemType item : list) {
			StandardVpcType tmp = BeanUtil.copyProperties(item, StandardVpcType.class);
			stdList.add(tmp);
		}
		return stdList;
	}

}