package com.gcloud.controller.network.handler.api.port;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.network.model.DetailNetworkInterfaceParams;
import com.gcloud.controller.network.service.IPortService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.network.model.DetailNic;
import com.gcloud.header.network.msg.api.ApiDetailNetworkInterfaceMsg;
import com.gcloud.header.network.msg.api.ApiDetailNetworkInterfaceReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@ApiHandler(module=Module.ECS,subModule=SubModule.NETWORKINTERFACE,action="DetailNetworkInterface",name="网卡详情")
public class ApiDetailNetworkInterfaceHandler extends MessageHandler<ApiDetailNetworkInterfaceMsg, ApiDetailNetworkInterfaceReplyMsg>{

	@Autowired
	private IPortService portService;
	
	@Override
	public ApiDetailNetworkInterfaceReplyMsg handle(ApiDetailNetworkInterfaceMsg msg) throws GCloudException {
		DetailNetworkInterfaceParams params = BeanUtil.copyProperties(msg, DetailNetworkInterfaceParams.class);
		DetailNic response = portService.detailNetworkInterface(params, msg.getCurrentUser());
		
		ApiDetailNetworkInterfaceReplyMsg reply = new ApiDetailNetworkInterfaceReplyMsg();
		reply.setDetailNetworkInterface(response);
		return reply;
	}

}