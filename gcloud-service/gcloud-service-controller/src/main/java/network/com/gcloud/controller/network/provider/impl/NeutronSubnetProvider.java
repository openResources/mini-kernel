package com.gcloud.controller.network.provider.impl;

import com.gcloud.common.util.NetworkUtil;
import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.network.dao.IpallocationPoolDao;
import com.gcloud.controller.network.dao.SubnetDao;
import com.gcloud.controller.network.entity.IpallocationPools;
import com.gcloud.controller.network.entity.Network;
import com.gcloud.controller.network.entity.Subnet;
import com.gcloud.controller.network.model.CreateSubnetParams;
import com.gcloud.controller.network.provider.ISubnetProvider;
import com.gcloud.controller.network.util.NeutronSubnetDhcpUtil;
import com.gcloud.controller.provider.NeutronProviderProxy;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.simpleflow.Flow;
import com.gcloud.core.simpleflow.FlowDoneHandler;
import com.gcloud.core.simpleflow.NoRollbackFlow;
import com.gcloud.core.simpleflow.SimpleFlowChain;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.enums.ProviderType;
import com.gcloud.header.enums.ResourceType;
import lombok.extern.slf4j.Slf4j;
import org.openstack4j.model.network.Pool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
@Component
public class NeutronSubnetProvider implements ISubnetProvider {

    @Autowired
    private NeutronProviderProxy proxy;

    @Autowired
    private SubnetDao subnetDao;

    @Autowired
    private IpallocationPoolDao ipallocationPoolDao;

    @Override
    public ResourceType resourceType() {
        return ResourceType.SUBNET;
    }

    @Override
    public ProviderType providerType() {
        return ProviderType.NEUTRON;
    }

    @Override
    public void createSubnet(Network network, String subnetId, CreateSubnetParams params, CurrentUser currentUser) {

        List<Subnet> subnets = subnetDao.findByProperty(Subnet.NETWORK_ID, network.getId());
        if(subnets != null && !subnets.isEmpty()){
            List<String> cirds = subnets.stream().map(Subnet::getCidr).collect(Collectors.toList());
            cirds.add(params.getCidrBlock());
            if(NetworkUtil.cidrConflict(cirds)){
                throw new GCloudException("0030105::子网冲突");
            }
        }

        SimpleFlowChain<org.openstack4j.model.network.Subnet, String> chain = new SimpleFlowChain<>("createSubnet");
        boolean dhcp = params.getDhcp() == null ? true : params.getDhcp();
        chain.then(new Flow<org.openstack4j.model.network.Subnet>("createSubnet") {
            @Override
            public void run(SimpleFlowChain chain, org.openstack4j.model.network.Subnet data) {
                org.openstack4j.model.network.Subnet net = proxy.createSubnet(network.getProviderRefId(), params.getSubnetName(), params.getCidrBlock(), params.getDnsNameServers(), params.getGatewayIp(), dhcp, params.getAllocationPools());
                chain.data(net);
                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, org.openstack4j.model.network.Subnet data) {
                proxy.deleteSubnet(data.getId());
                chain.rollback();
            }

        }).then(new Flow<org.openstack4j.model.network.Subnet>("save to db", true) {
            @Override
            public void run(SimpleFlowChain chain, org.openstack4j.model.network.Subnet data) {
                Subnet subnet = new Subnet();
                subnet.setId(subnetId);
                subnet.setCidr(params.getCidrBlock());
                subnet.setName(params.getSubnetName());
                subnet.setUserId(currentUser.getId());
                subnet.setCreateTime(new Date());
                subnet.setNetworkId(network.getId());
                subnet.setZoneId(params.getZoneId());
                subnet.setProvider(providerType().getValue());
                subnet.setProviderRefId(data.getId());
                subnet.setTenantId(currentUser.getDefaultTenant());
                subnet.setEnableDhcp(params.getDhcp() == null ? false : params.getDhcp());

                //dns
                if (params.getDnsNameServers() != null) {
                	String dnsServers = "";
        			for (String host : params.getDnsNameServers()) {
        				dnsServers += host + ";";
        			}
        			if(StringUtils.isNotBlank(dnsServers)) {
        				subnet.setDnsServers(dnsServers.substring(0, dnsServers.length() - 1));
        			}
        		}
                if(com.gcloud.common.util.StringUtils.isNotBlank(params.getGatewayIp())) {
                	subnet.setGatewayIp(params.getGatewayIp());
                }

                if(data.getAllocationPools() != null){
                    for(Pool pool : data.getAllocationPools()){
                        IpallocationPools allocationPool = new IpallocationPools();
                        allocationPool.setId(UUID.randomUUID().toString());
                        allocationPool.setSubnetId(subnetId);
                        allocationPool.setFirstIp(pool.getStart());
                        allocationPool.setLastIp(pool.getEnd());

                        ipallocationPoolDao.save(allocationPool);
                    }
                }

                subnetDao.save(subnet);


                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, org.openstack4j.model.network.Subnet data) {
                ipallocationPoolDao.deleteBySubnetId(subnetId);
                subnetDao.deleteById(subnetId);
            }
        }).then(new NoRollbackFlow<org.openstack4j.model.network.Subnet>() {
            @Override
            public void run(SimpleFlowChain chain, org.openstack4j.model.network.Subnet data) {

                //dhcp
                if(dhcp){
                    configDhcpPort(data.getNetworkId(), data.getId(), subnetId, currentUser);
                }
                chain.next();

            }

        }).done(new FlowDoneHandler<org.openstack4j.model.network.Subnet>() {
            @Override
            public void handle(org.openstack4j.model.network.Subnet data) {
                chain.setResult(data.getId());
            }
        }).start();

        if (StringUtils.isNotBlank(chain.getErrorCode())) {
            throw new GCloudException(chain.getErrorCode());
        }


    }

    private void configDhcpPort(String networkId, String subnetId, String gcloudSubnetId, CurrentUser currentUser){
        NeutronSubnetDhcpUtil.createOrUpdateHandle(gcloudSubnetId, subnetId);
    }

    @Override
    public void deleteSubnet(String subnetRefId) {
        proxy.deleteSubnet(subnetRefId);
    }

    @Override
    public void modifyAttribute(Subnet subnet, String subnetName, List<String> dnsNameservers, String gatewayIp, Boolean dhcp, CurrentUser currentUser) {

        SimpleFlowChain<org.openstack4j.model.network.Subnet, String> chain = new SimpleFlowChain<>("modifySubnetAttribute");
        chain.then(new Flow<org.openstack4j.model.network.Subnet>() {
            @Override
            public void run(SimpleFlowChain chain, org.openstack4j.model.network.Subnet data) {
                org.openstack4j.model.network.Subnet neutronSubnet = proxy.modifySubnetAttribute(subnet.getProviderRefId(), subnetName, dnsNameservers, gatewayIp, dhcp);
                chain.data(neutronSubnet);
                chain.next();
            }

            @Override
            public void rollback(SimpleFlowChain chain, org.openstack4j.model.network.Subnet data) {
                List<String> orgDnsServer = new ArrayList<>();
                if(!StringUtils.isBlank(subnet.getDnsServers())){
                    orgDnsServer = Arrays.asList(subnet.getDnsServers().split(","));
                }

                proxy.modifySubnetAttribute(subnet.getProviderRefId(), subnet.getName(), orgDnsServer, subnet.getGatewayIp(), subnet.getEnableDhcp());
                chain.rollback();
            }
        }).then(new NoRollbackFlow<org.openstack4j.model.network.Subnet>() {
            @Override
            public void run(SimpleFlowChain chain, org.openstack4j.model.network.Subnet data) {
                //启用dhcp
                if(dhcp != null && !subnet.getEnableDhcp() && dhcp){
                    configDhcpPort(data.getNetworkId(), data.getId(), subnet.getId(), currentUser);
                }
                chain.next();

                //如果后面会还有流程，�?要处�?  取消dhcp后，回滚导致的，回滚出来的port是新的port�?

            }

        }).start();

        if (StringUtils.isNotBlank(chain.getErrorCode())) {
            throw new GCloudException(chain.getErrorCode());
        }

    }

    @Override
    public List<Subnet> list(Map<String, String> filter) {
        List<org.openstack4j.model.network.Subnet> subnets = proxy.listSubnet(filter);
        List<Subnet> retList = new ArrayList<>();
        for (org.openstack4j.model.network.Subnet s : subnets) {
            Subnet sn = new Subnet();
            sn.setCidr(s.getCidr());
            sn.setId(s.getId());
            sn.setName(s.getName());
            sn.setNetworkId(s.getNetworkId());
//            sn.setRouterId();
            sn.setUpdatedAt(s.getUpdatedAt());
            // TODO: other items.

            retList.add(sn);
        }

        return retList;
    }

}