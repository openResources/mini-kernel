package com.gcloud.controller.compute.handler.api.vm.zone;

import com.gcloud.controller.compute.handler.api.model.DescribeZonesParams;
import com.gcloud.controller.compute.service.vm.zone.IVmZoneService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.model.AvailableZone;
import com.gcloud.header.compute.msg.api.vm.zone.ApiDescribeZonesMsg;
import com.gcloud.header.compute.msg.api.vm.zone.ApiDescribeZonesReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@ApiHandler(module = Module.ECS, subModule = SubModule.VM, action = "DescribeZones", name = "可用区列�?")
public class ApiDescribeZonesHandler extends MessageHandler<ApiDescribeZonesMsg, ApiDescribeZonesReplyMsg> {

    @Autowired
    private IVmZoneService zoneService;

    @Override
    public ApiDescribeZonesReplyMsg handle(ApiDescribeZonesMsg msg) throws GCloudException {
//        DescribeZonesParams params = BeanUtil.copyProperties(msg, DescribeZonesParams.class);
    	DescribeZonesParams params = new DescribeZonesParams();
    	//如果使用copyProperties复制的话，如果enable为空，会复制为false，不利用后面判断是否有传enable的处�?
    	params.setPoolId(msg.getPoolId());
    	params.setEnabled(msg.getEnabled());
    	params.setPageNumber(msg.getPageNumber());
    	params.setPageSize(msg.getPageSize());
    	params.setDiskCategoryId(msg.getDiskCategoryId());
        params.setSimple(msg.getSimple());
    	
        PageResult<AvailableZone> page = this.zoneService.describeZones(params);
        ApiDescribeZonesReplyMsg replyMsg = new ApiDescribeZonesReplyMsg();
        replyMsg.init(page);
        return replyMsg;
    }

}