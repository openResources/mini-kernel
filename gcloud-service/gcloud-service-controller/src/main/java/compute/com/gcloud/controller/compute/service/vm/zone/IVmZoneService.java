package com.gcloud.controller.compute.service.vm.zone;

import com.gcloud.controller.compute.handler.api.model.DescribeZonesParams;
import com.gcloud.controller.compute.model.vm.CreateZoneParams;
import com.gcloud.controller.compute.model.vm.DeleteZoneParams;
import com.gcloud.controller.compute.model.vm.DetailZoneParams;
import com.gcloud.controller.compute.model.vm.EnableZoneParams;
import com.gcloud.controller.compute.model.vm.UpdateZoneParams;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.msg.api.model.AvailableZone;
import com.gcloud.header.compute.msg.api.model.DetailZone;
import com.gcloud.header.compute.msg.api.model.GetZoneComputeNodesResponse;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public interface IVmZoneService {

    PageResult<AvailableZone> describeZones(DescribeZonesParams params);

    String createZone(CreateZoneParams params);

    void updateComputeNodeZone(String zoneId, List<Integer> nodeIds);

    void deleteZone(DeleteZoneParams params, CurrentUser currentUser);
    
    DetailZone detailZone(DetailZoneParams params, CurrentUser currentUser);
    
    void enableZone(EnableZoneParams params);
    
    void updateZone(UpdateZoneParams params);
    
    String setZoneComputeNodes(String zoneId, List<String> hostNames);
    
    GetZoneComputeNodesResponse getZoneHostNames(String zoneId);
}