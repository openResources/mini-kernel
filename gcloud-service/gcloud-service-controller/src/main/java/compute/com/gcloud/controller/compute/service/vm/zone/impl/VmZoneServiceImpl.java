package com.gcloud.controller.compute.service.vm.zone.impl;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.compute.dao.ComputeNodeDao;
import com.gcloud.controller.compute.dao.InstanceDao;
import com.gcloud.controller.compute.dao.ZoneDao;
import com.gcloud.controller.compute.dao.ZoneInstanceTypeDao;
import com.gcloud.controller.compute.entity.AvailableZoneEntity;
import com.gcloud.controller.compute.entity.ComputeNode;
import com.gcloud.controller.compute.entity.VmInstance;
import com.gcloud.controller.compute.entity.ZoneInstanceTypeEntity;
import com.gcloud.controller.compute.handler.api.model.DescribeInstanceTypesParams;
import com.gcloud.controller.compute.handler.api.model.DescribeZonesParams;
import com.gcloud.controller.compute.model.vm.CreateZoneParams;
import com.gcloud.controller.compute.model.vm.DeleteZoneParams;
import com.gcloud.controller.compute.model.vm.DetailZoneParams;
import com.gcloud.controller.compute.model.vm.EnableZoneParams;
import com.gcloud.controller.compute.model.vm.UpdateZoneParams;
import com.gcloud.controller.compute.service.vm.base.IVmBaseService;
import com.gcloud.controller.compute.service.vm.zone.IVmZoneService;
import com.gcloud.controller.compute.utils.RedisNodesUtil;
import com.gcloud.controller.storage.dao.DiskCategoryZoneDao;
import com.gcloud.controller.storage.dao.VolumeDao;
import com.gcloud.controller.storage.entity.DiskCategoryZone;
import com.gcloud.controller.storage.service.IStoragePoolService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.enums.ZoneState;
import com.gcloud.header.compute.msg.api.model.AvailableZone;
import com.gcloud.header.compute.msg.api.model.DetailZone;
import com.gcloud.header.compute.msg.api.model.GetZoneComputeNodesResponse;
import com.gcloud.header.compute.msg.api.model.InstanceTypeItemType;
import com.gcloud.header.compute.msg.api.vm.zone.AvailableResource;
import com.gcloud.header.compute.msg.api.vm.zone.AvailableResources;
import com.gcloud.header.compute.msg.api.vm.zone.DataDiskCategories;
import com.gcloud.header.compute.msg.api.vm.zone.DiskCategory;
import com.gcloud.header.compute.msg.api.vm.zone.InstanceType;
import com.gcloud.header.compute.msg.api.vm.zone.InstanceTypes;
import com.gcloud.header.compute.msg.api.vm.zone.SystemDiskCategories;
import com.gcloud.header.storage.model.DiskCategoryModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Service
@Slf4j
public class VmZoneServiceImpl implements IVmZoneService {

    @Autowired
    private ZoneDao zoneDao;

    @Autowired
    private ComputeNodeDao nodeDao;

    @Autowired
    private IVmBaseService vmBaseService;

    @Autowired
    private IStoragePoolService poolService;

    @Autowired
    private ZoneInstanceTypeDao zoneInstanceTypeDao;
    @Autowired
    private DiskCategoryZoneDao diskCategoryZoneDao;

    @Autowired
    private InstanceDao instanceDao;

    @Autowired
    private VolumeDao volumeDao;

    @Override
    public PageResult<AvailableZone> describeZones(DescribeZonesParams params) {

        PageResult page = this.zoneDao.page(params);
        List<AvailableZone> vos = new ArrayList<>();
        for (Object obj : page.getList()) {
            AvailableZoneEntity entity = (AvailableZoneEntity) obj;
            AvailableZone vo = new AvailableZone();
            vo.setZoneId(entity.getId());
            vo.setLocalName(entity.getName());
            vo.setStatus(entity.isEnabled());
            vo.setCnStatus(ZoneState.getCnName(entity.isEnabled()));
            vo.setEnabled(entity.isEnabled());
            vos.add(vo);
        }

        if (params.getSimple() == null || !params.getSimple()) {
            //初始化可用资�?
            for (AvailableZone zone : vos) {
                zone.setAvailableResources(new AvailableResources());
                zone.getAvailableResources().setResourcesInfo(new ArrayList<>());
                AvailableResource resource = new AvailableResource();

                resource.setInstanceTypes(new InstanceTypes());
                resource.getInstanceTypes().setSupportedInstanceType(new ArrayList<>());
                DescribeInstanceTypesParams tmpP = new DescribeInstanceTypesParams();
                tmpP.setZoneId(zone.getZoneId());
                for (InstanceTypeItemType type : this.vmBaseService.describeInstanceTypes(tmpP).getList()) {
                    InstanceType resType = new InstanceType();
                    resType.setInstanceTypeId(type.getInstanceTypeId());
                    resType.setInstanceTypeName(type.getInstanceTypeName());
                    resource.getInstanceTypes().getSupportedInstanceType().add(resType);
                }


                resource.setSystemDiskCategories(new SystemDiskCategories());
                resource.getSystemDiskCategories().setSupportedSystemDiskCategory(new ArrayList<>());
                resource.setDataDiskCategories(new DataDiskCategories());
                resource.getDataDiskCategories().setSupportedDataDiskCategory(new ArrayList<>());
                for (DiskCategoryModel tmp : this.poolService.describeDiskCategories(zone.getZoneId())) {
                    DiskCategory diskCategory = new DiskCategory();
                    diskCategory.setDiskTypeId(tmp.getId());
                    diskCategory.setDiskTypeName(tmp.getName());
                    diskCategory.setDiskTypeCnName(tmp.getName());
                    diskCategory.setMin(tmp.getMinSize());
                    diskCategory.setMax(tmp.getMaxSize());
                    resource.getSystemDiskCategories().getSupportedSystemDiskCategory().add(diskCategory);
                    resource.getDataDiskCategories().getSupportedDataDiskCategory().add(diskCategory);
                }

                zone.getAvailableResources().getResourcesInfo().add(resource);
            }
        }

        page.setList(vos);
        return page;

    }


    @Override
    public String createZone(CreateZoneParams params) {
        String zoneId = null;
        synchronized (VmZoneServiceImpl.class) {
            String zoneName = params.getZoneName();
            if (this.zoneDao.findUniqueByProperty("name", zoneName) != null) {
                log.error("0180102::可用区名称已存在");
                throw new GCloudException("0180102::可用区名称已存在");
            }
            AvailableZoneEntity zone = new AvailableZoneEntity();
            zoneId = StringUtils.genUuid();
            zone.setId(zoneId);
            zone.setName(zoneName);
            zone.setEnabled(params.isEnabled());
            try {
                this.zoneDao.save(zone);
            } catch (Exception e) {
                log.error("创建可用区失败，原因：�??" + e.getCause() + "::" + e.getMessage() + "�?");
                throw new GCloudException("0180103::创建可用区失�?");
            }

        }
        return zoneId;
    }

    @Override
    public void updateComputeNodeZone(String zoneId, List<Integer> nodeIds) {
        AvailableZoneEntity zone = this.zoneDao.getById(zoneId);
        if (zone == null) {
            log.error("0180502::不存在该可用�?");
            throw new GCloudException("0180502::不存在该可用�?");
        }
        if (nodeIds != null) {
            for (Integer nodeId : nodeIds) {
                ComputeNode node = this.nodeDao.getById(nodeId);
                if (node != null) {
                    node.setZoneId(zoneId);
                    List<String> updateField = new ArrayList<>();
                    updateField.add("zoneId");
                    this.nodeDao.update(node, updateField);
                    RedisNodesUtil.updateComputeNodeZone(node.getHostname(), zoneId);
                }
            }
        }
    }

    @Override
    public void deleteZone(DeleteZoneParams params, CurrentUser currentUser) {
        AvailableZoneEntity zone = zoneDao.getById(params.getId());
        if (zone == null) {
            log.error("0180202::不存在该可用�?");
            throw new GCloudException("0180202::不存在该可用�?");
        }
        //TODO 正在使用不能操作
        String zoneId = zone.getId();
        Map<String, Object> zoneInstanceParamas = new HashMap<>();
        zoneInstanceParamas.put(ZoneInstanceTypeEntity.ZONE_ID, zoneId);
        List<ZoneInstanceTypeEntity> zoneInstanceList = zoneInstanceTypeDao.findByProperties(zoneInstanceParamas);
        if (zoneInstanceList != null && !zoneInstanceList.isEmpty()) {
            log.error("0180204::计算规格与可用区已关联，暂无法删�?");
            throw new GCloudException("0180204::计算规格与可用区已关联，暂无法删�?");
        }

        Map<String, Object> diskCategoryParams = new HashMap<>();
        diskCategoryParams.put(DiskCategoryZone.ZONE_ID, zoneId);
        List<DiskCategoryZone> zoneDiskCategoryList = diskCategoryZoneDao.findByProperties(diskCategoryParams);
        if (zoneDiskCategoryList != null && !zoneDiskCategoryList.isEmpty()) {
            log.error("0180205::磁盘类型与可用区已关联，暂无法删�?");
            throw new GCloudException("0180205::磁盘类型与可用区已关联，暂无法删�?");
        }

        try {
            zoneDao.deleteById(params.getId());
        } catch (Exception e) {
            log.error("删除可用区失败，原因：�??" + e.getCause() + "::" + e.getMessage() + "�?");
            throw new GCloudException("0180203::删除可用区失�?");
        }
    }

    @Override
    public DetailZone detailZone(DetailZoneParams params, CurrentUser currentUser) {
        AvailableZoneEntity zone = zoneDao.getById(params.getZoneId());

        DetailZone response = new DetailZone();
        if (zone == null) {
            log.error("::不存在该可用�?");
            throw new GCloudException("::不存在该可用�?");
        }

        //可用区资源的数据获取
        AvailableResource resource = new AvailableResource();
        resource.setInstanceTypes(new InstanceTypes());
        resource.getInstanceTypes().setSupportedInstanceType(new ArrayList<>());

        DescribeInstanceTypesParams instanceTypeParams = new DescribeInstanceTypesParams();
        instanceTypeParams.setZoneId(zone.getId());
        for (InstanceTypeItemType type : this.vmBaseService.describeInstanceTypes(instanceTypeParams).getList()) {
            InstanceType resType = new InstanceType();
            resType.setInstanceTypeId(type.getInstanceTypeId());
            resType.setInstanceTypeName(type.getInstanceTypeName());
            resource.getInstanceTypes().getSupportedInstanceType().add(resType);
        }

        resource.setSystemDiskCategories(new SystemDiskCategories());
        resource.getSystemDiskCategories().setSupportedSystemDiskCategory(new ArrayList<>());
        resource.setDataDiskCategories(new DataDiskCategories());
        resource.getDataDiskCategories().setSupportedDataDiskCategory(new ArrayList<>());
        for (DiskCategoryModel item : poolService.describeDiskCategories(zone.getId())) {
            DiskCategory diskCategory = new DiskCategory();
            diskCategory.setDiskTypeId(item.getId());
            diskCategory.setDiskTypeName(item.getName());
            diskCategory.setDiskTypeCnName(item.getName());
            diskCategory.setMin(item.getMinSize());
            diskCategory.setMax(item.getMaxSize());
            resource.getSystemDiskCategories().getSupportedSystemDiskCategory().add(diskCategory);
            resource.getDataDiskCategories().getSupportedDataDiskCategory().add(diskCategory);
        }

        //其他数据的设�?
        response.setZoneId(zone.getId());
        response.setLocalName(zone.getName());
        response.setAvailableResource(resource);
        response.setEnabled(zone.isEnabled());
        response.setCnStatus(ZoneState.getCnName(zone.isEnabled()));

        return response;
    }

    @Override
    public void enableZone(EnableZoneParams params) {
        String zoneId = params.getZoneId();
        Boolean enabled = params.getEnabled();

        AvailableZoneEntity zone = zoneDao.getById(zoneId);
        if (null == zone) {
            log.error("::不存在该可用�?");
            throw new GCloudException("::不存在该可用�?");
        }

        Boolean zoneEnabled = zone.isEnabled();
        if (!zoneEnabled.equals(enabled)) {
            List<String> zoneField = new ArrayList<>();
            zoneField.add(zone.updateEnabled(enabled));
            try {
                zoneDao.update(zone, zoneField);
            } catch (Exception e) {
                log.error("启用或禁用可用区失败，原因：�?" + e.getCause() + "::" + e.getMessage() + "�?");
                throw new GCloudException("::启用或禁用可用区失败");
            }
        }

    }

    @Override
    public void updateZone(UpdateZoneParams params) {
        String zoneId = params.getZoneId();
        AvailableZoneEntity zone = zoneDao.getById(zoneId);
        if (null == zone) {
            log.error("::不存在该可用�?");
            throw new GCloudException("::不存在该可用�?");
        }

        List<String> zoneField = new ArrayList<>();
        zoneField.add(zone.updateName(params.getZoneName()));
        try {
            zoneDao.update(zone, zoneField);
        } catch (Exception e) {
            log.error("编辑可用区失败，原因：�??" + e.getCause() + "::" + e.getMessage() + "�?");
            throw new GCloudException("::编辑可用区失�?");
        }

    }


    @Override
    public String setZoneComputeNodes(String zoneId, List<String> hostNames) {
        AvailableZoneEntity zone = this.zoneDao.getById(zoneId);
        if (zone == null) {
            log.error("0200102::不存在该可用�?");
            throw new GCloudException("0200102::不存在该可用�?");
        }
        List<ComputeNode> zoneNodes = nodeDao.findByProperty("zoneId", zoneId);
        List<String> zoneHosts = zoneNodes.stream().map(p -> p.getHostname()).collect(Collectors.toList());
        List<String> allocateHosts = new ArrayList<>();
        if (hostNames != null) {

            for (String hostName : hostNames) {
                ComputeNode node = this.nodeDao.findUniqueByProperty("hostname", hostName);
                if (node != null) {
                    if (zoneHosts.contains(node.getHostname())) {
                        zoneHosts.remove(node.getHostname());
                        continue;
                    }

                    allocateHosts.add(node.getHostname());
                    checkHostZone(node, hostName, true);
                }
            }
        }

        //新增的上面已经校验，只校验移除的
        for (String hostname : zoneHosts) {
            ComputeNode node = this.nodeDao.findUniqueByProperty("hostname", hostname);
            checkHostZone(node, hostname, false);
        }

        //不进行所有回滚，以为redis回滚可能无法全部回滚，导致数据异�?
        for (String hostname : allocateHosts) {
            allocateZone(hostname, zoneId);
        }

        for (String hostname : zoneHosts) {
            unsetZone(hostname);
        }
        return zone.getName();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public boolean allocateZone(String hostname, String zoneId) {
        int result = nodeDao.allocateZone(hostname, zoneId);
        if (result > 0) {
            poolService.updateLocalPoolZone(hostname, zoneId);
            RedisNodesUtil.updateComputeNodeZone(hostname, zoneId);
        }
        return result > 0;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void unsetZone(String hostname) {
        nodeDao.clearNodeZone(hostname);
        //清除本地存储池可用区
        poolService.clearLocalPoolZone(hostname);
        RedisNodesUtil.updateComputeNodeZone(hostname, "");
    }

    private void checkHostZone(ComputeNode node, String zoneId, Boolean create){

        String hostName = node.getHostname();
        if (create && StringUtils.isNotBlank(node.getZoneId()) && !node.getZoneId().equals(zoneId)) {
            throw new GCloudException("0200104::节点[%s]已经关联了可用区", hostName);
        }

        VmInstance vmInstance = instanceDao.findOneByProperty(VmInstance.HOSTNAME, hostName);
        if (vmInstance != null) {
            throw new GCloudException("0200105::节点[%s]上有云服务器，不能切换可用区", hostName);
        }

        if (volumeDao.hasLocalVolume(hostName)) {
            throw new GCloudException("0200106::节点[%s]上有磁盘，不能切换可用区", hostName);
        }
    }

    @Override
    public GetZoneComputeNodesResponse getZoneHostNames(String zoneId) {
        List<ComputeNode> nodes = nodeDao.findByProperty("zoneId", zoneId);
        List<String> hostNames = nodes.stream().map(p -> p.getHostname()).collect(Collectors.toList());
        GetZoneComputeNodesResponse res = new GetZoneComputeNodesResponse();
        res.setZoneId(zoneId);
        res.setHostNames(hostNames);
        return res;
    }

}