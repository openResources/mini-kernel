package com.gcloud.controller.compute.handler.api.vm.network;

import com.gcloud.controller.ResourceIsolationCheck;
import com.gcloud.controller.compute.service.vm.netowork.IVmNetworkService;
import com.gcloud.controller.compute.workflow.model.network.AttachPortInitFlowCommandRes;
import com.gcloud.controller.compute.workflow.model.network.AttachPortWorkflowReq;
import com.gcloud.controller.compute.workflow.vm.network.AttachPortWorkflow;
//import com.gcloud.controller.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.controller.workflow.BaseWorkFlowHandler;
import com.gcloud.header.ApiReplyMessage;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.compute.msg.api.network.ApiAttachNetworkInterfaceMsg;
import com.gcloud.header.log.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@LongTask
@GcLog(isMultiLog = true, taskExpect = "挂载网卡")
@ApiHandler(module= Module.ECS, subModule = SubModule.NETWORKINTERFACE, action="AttachNetworkInterface")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.INSTANCE, resourceIdField = "instanceId")
@ResourceIsolationCheck(resourceIsolationCheckType = ResourceIsolationCheckType.PORT, resourceIdField = "networkInterfaceId")
public class ApiAttachNetworkInterfaceHandler extends BaseWorkFlowHandler<ApiAttachNetworkInterfaceMsg, ApiReplyMessage> {

    @Autowired
    private IVmNetworkService vmNetworkService;

    @Override
    public Object preProcess(ApiAttachNetworkInterfaceMsg msg) throws GCloudException {
        return null;
    }

    @Override
    public ApiReplyMessage process(ApiAttachNetworkInterfaceMsg msg) throws GCloudException {
        ApiReplyMessage replyMessage = new ApiReplyMessage();
        AttachPortInitFlowCommandRes res = getFlowTaskFirstStepFirstRes(msg.getTaskId(), AttachPortInitFlowCommandRes.class);

        replyMessage.getTasks().add(Task.builder().taskId(res.getTaskId()).objectId(msg.getInstanceId())
        		.objectName(CacheContainer.getInstance().getString(CacheType.INSTANCE_ALIAS, msg.getInstanceId())).expect("挂载网卡成功").build());

        replyMessage.setTaskId(res.getTaskId());
        return replyMessage;

    }

    @Override
    public Class getWorkflowClass() {
        return AttachPortWorkflow.class;
    }

    @Override
    public Object initParams(ApiAttachNetworkInterfaceMsg msg) {
        AttachPortWorkflowReq req = new AttachPortWorkflowReq();
        req.setInstanceId(msg.getInstanceId());
        req.setNetworkInterfaceId(msg.getNetworkInterfaceId());
        req.setInTask(false);
        return req;
    }
}