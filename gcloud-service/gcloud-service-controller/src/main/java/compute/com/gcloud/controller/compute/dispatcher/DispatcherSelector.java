package com.gcloud.controller.compute.dispatcher;

import java.util.Arrays;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public enum DispatcherSelector {
    SIMPLE("simple", SimpleDispatcher.class),
    POLLING("polling", PollingDispatcher.class),
    RANDOM("random", RandomDispatcher.class);

    DispatcherSelector(String value, Class<?> impl) {
        this.value = value;
        this.impl = impl;
    }

    private String value;
    private Class<?> impl;

    public static DispatcherSelector value(String value){
        return Arrays.stream(DispatcherSelector.values()).filter(s -> s.getValue().equals(value)).findFirst().orElse(null);
    }

    public String getValue() {
        return value;
    }

    public Class<?> getImpl() {
        return impl;
    }
}