package com.gcloud.controller.compute.workflow.vm.create;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.compute.workflow.model.vm.ModifyInstancePasswordFlowCommandReq;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.header.compute.msg.node.vm.base.ModifyPasswordMsg;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Component
@Scope("prototype")
@Slf4j
public class ModifyInstancePasswordFlowCommand extends BaseWorkFlowCommand {
	@Autowired
	private MessageBus bus;
	
	@Override
	protected Object process() throws Exception {
		ModifyInstancePasswordFlowCommandReq req = (ModifyInstancePasswordFlowCommandReq)getReqParams();
		
		ModifyPasswordMsg modifyMsg = new ModifyPasswordMsg();
    	modifyMsg.setInstanceId(req.getInstanceId());
    	modifyMsg.setServiceId(MessageUtil.computeServiceId(req.getCreateHost()));
    	modifyMsg.setPassword(req.getPassword());
    	modifyMsg.setTaskId(getTaskId());
        bus.send(modifyMsg);
		return null;
	}
	
	public boolean judgeExecute() {
		ModifyInstancePasswordFlowCommandReq req = (ModifyInstancePasswordFlowCommandReq)getReqParams();
		if(req.isIsoCreate() || StringUtils.isBlank(req.getPassword())) {
			return false;
		}
		return true;
	}

	@Override
	protected Object rollback() throws Exception {
		return null;
	}

	@Override
	protected Object timeout() throws Exception {
		return null;
	}

	@Override
	protected Class<?> getReqParamClass() {
		return ModifyInstancePasswordFlowCommandReq.class;
	}

	@Override
	protected Class<?> getResParamClass() {
		return null;
	}
	
	@Override
	public int getTimeOut() {
		return 660;
	}

}