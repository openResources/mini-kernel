package com.gcloud.controller.compute.workflow.vm.trash;

import com.gcloud.common.util.StringUtils;
import com.gcloud.controller.compute.workflow.model.trash.ForceDetachAndDeleteNetcardWorkflowReq;
import com.gcloud.core.workflow.core.BaseWorkFlows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
@Component
@Scope("prototype")
@Slf4j
public class ForceDetachAndDeleteNetcardWorkflow extends BaseWorkFlows {

    @Override
    public String getFlowTypeCode() {
        return "forceDetachAndDeleteNetcardWorkflow";
    }

    @Override
    public Object preProcess() {
    	return null;
    }

    @Override
    public void process() {

    }

    @Override
    public boolean judgeExecute() {
        ForceDetachAndDeleteNetcardWorkflowReq req = (ForceDetachAndDeleteNetcardWorkflowReq)getReqParams();
        return req != null && req.getRepeatParams() != null && StringUtils.isNotBlank(req.getRepeatParams().getNetcardId());
    }

    @Override
    protected Class<?> getReqParamClass() {
        return ForceDetachAndDeleteNetcardWorkflowReq.class;
    }
}