package com.gcloud.controller.compute.workflow.model.vm;

import com.gcloud.controller.compute.model.vm.VmImageInfo;
import com.gcloud.core.workflow.model.WorkflowFirstStepResException;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.compute.msg.api.model.DiskInfo;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class CreateInstanceFlowInitCommandRes extends WorkflowFirstStepResException{
	private String instanceId;
	private String createHost;
	private String storageType;

	private String instancePath;

	private int remotePort;

	private List<DiskInfo> repeatParams;

	private DiskInfo systemDisk;

	private VmImageInfo imageInfo;

	private Integer cpu;
	private Integer memory;
	private Integer systemDiskSize;
	private List<DiskInfo> dataDisk;
	private String isoId;
	
	private String portName;
	private boolean isoCreate;

	public String getIsoId() {
		return isoId;
	}

	public void setIsoId(String isoId) {
		this.isoId = isoId;
	}

	public String getPortName() {
		return portName;
	}

	public void setPortName(String portName) {
		this.portName = portName;
	}

	// 用于记录日志
	private String taskId;
	private String instanceName;
	private CurrentUser createUser;

	public DiskInfo getSystemDisk() {
		return systemDisk;
	}

	public void setSystemDisk(DiskInfo systemDisk) {
		this.systemDisk = systemDisk;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getCreateHost() {
		return createHost;
	}

	public void setCreateHost(String createHost) {
		this.createHost = createHost;
	}

	public String getInstancePath() {
		return instancePath;
	}

	public void setInstancePath(String instancePath) {
		this.instancePath = instancePath;
	}

	public int getRemotePort() {
		return remotePort;
	}

	public void setRemotePort(int remotePort) {
		this.remotePort = remotePort;
	}

	public List<DiskInfo> getRepeatParams() {
		return repeatParams;
	}

	public void setRepeatParams(List<DiskInfo> repeatParams) {
		this.repeatParams = repeatParams;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

    public String getStorageType() {
        return storageType;
    }

    public void setStorageType(String storageType) {
        this.storageType = storageType;
    }

    public VmImageInfo getImageInfo() {
        return imageInfo;
    }

    public void setImageInfo(VmImageInfo imageInfo) {
        this.imageInfo = imageInfo;
    }

	public Integer getCpu() {
		return cpu;
	}

	public void setCpu(Integer cpu) {
		this.cpu = cpu;
	}

	public Integer getMemory() {
		return memory;
	}

	public void setMemory(Integer memory) {
		this.memory = memory;
	}

	public Integer getSystemDiskSize() {
		return systemDiskSize;
	}

	public void setSystemDiskSize(Integer systemDiskSize) {
		this.systemDiskSize = systemDiskSize;
	}

	public List<DiskInfo> getDataDisk() {
		return dataDisk;
	}

	public void setDataDisk(List<DiskInfo> dataDisk) {
		this.dataDisk = dataDisk;
	}

	public CurrentUser getCreateUser() {
		return createUser;
	}

	public void setCreateUser(CurrentUser createUser) {
		this.createUser = createUser;
	}

	public boolean isIsoCreate() {
		return isoCreate;
	}

	public void setIsoCreate(boolean isoCreate) {
		this.isoCreate = isoCreate;
	}

}