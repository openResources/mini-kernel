package com.gcloud.controller.log.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcloud.controller.log.model.LogFeedbackParams;
import com.gcloud.controller.log.service.ILogService;
import com.gcloud.core.workflow.service.IFlowTaskFeedbackLogService;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Service
public class FlowTaskFeedbackLogService implements IFlowTaskFeedbackLogService{
	@Autowired
	ILogService logService;

	@Override
	public void flowTaskFeedbackLog(String taskId, String objectId, String status, String code) {
		LogFeedbackParams params = new LogFeedbackParams();
		params.setTaskId(taskId);
		params.setStatus(status);
		params.setCode(code);
		logService.feedback(params);
	}

}