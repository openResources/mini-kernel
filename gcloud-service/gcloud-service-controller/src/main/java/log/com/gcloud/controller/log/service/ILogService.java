package com.gcloud.controller.log.service;

import com.gcloud.controller.log.entity.Log;
import com.gcloud.controller.log.model.LogFeedbackParams;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.GMessage;
import com.gcloud.header.ReplyMessage;
import com.gcloud.header.log.LogRecordMsg;
import com.gcloud.header.log.model.LogAttributesType;
import com.gcloud.header.log.msg.api.ApiDescribeLogsMsg;

import java.util.Date;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface ILogService {
	Long save(Log log);
	
	void recordLog(GMessage message, MessageHandler handler, GCloudException ge, Date startTime);
	
	void recordMultiLog(GMessage message, MessageHandler handler, GCloudException ge, Date startTime, ReplyMessage reply);
	
	void feedback(LogFeedbackParams params);

    void feedbackAsync(LogFeedbackParams params);
	
	void logRecord(LogRecordMsg msg);
	
	PageResult<LogAttributesType> describeLogs(ApiDescribeLogsMsg msg);
}