package com.gcloud.controller.image.handler.api.standard;

import org.springframework.beans.factory.annotation.Autowired;

import com.gcloud.controller.image.service.IImageService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.header.ApiVersion;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.image.msg.api.standard.StandardApiDeleteImageMsg;
import com.gcloud.header.image.msg.api.standard.StandardApiDeleteImageReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@GcLog(taskExpect = "删除镜像")
@ApiHandler(module= Module.ECS, subModule=SubModule.IMAGE, action="DeleteImage", versions = {ApiVersion.Standard})
public class StandardApiDeleteImageHandler extends MessageHandler<StandardApiDeleteImageMsg, StandardApiDeleteImageReplyMsg>{

	@Autowired
    private IImageService imageService;
	
	@Override
	public StandardApiDeleteImageReplyMsg handle(StandardApiDeleteImageMsg msg) throws GCloudException {
		imageService.deleteImage(msg.getImageId());
        msg.setObjectId(msg.getImageId());
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.IMAGE_NAME, msg.getImageId()));
        return new StandardApiDeleteImageReplyMsg();
	}

}