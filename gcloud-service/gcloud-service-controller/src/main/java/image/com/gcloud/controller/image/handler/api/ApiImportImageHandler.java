package com.gcloud.controller.image.handler.api;

import com.gcloud.controller.image.model.CreateImageParams;
import com.gcloud.controller.image.service.IImageService;
import com.gcloud.core.annotations.CustomAnnotations.GcLog;
import com.gcloud.core.annotations.CustomAnnotations.LongTask;
import com.gcloud.core.cache.container.CacheContainer;
import com.gcloud.core.cache.enums.CacheType;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.ApiHandler;
import com.gcloud.core.handle.MessageHandler;
import com.gcloud.core.util.BeanUtil;
import com.gcloud.header.Module;
import com.gcloud.header.SubModule;
import com.gcloud.header.api.ApiTypeAnno;
import com.gcloud.header.common.ApiTypeConst;
import com.gcloud.header.image.msg.api.ApiImportImageMsg;
import com.gcloud.header.image.msg.api.ApiImportImageReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@LongTask
@GcLog(taskExpect = "导入镜像")
@ApiHandler(module= Module.ECS, subModule=SubModule.IMAGE, action="ImportImage",name="导入镜像")
public class ApiImportImageHandler extends MessageHandler<ApiImportImageMsg, ApiImportImageReplyMsg> {

    @Autowired
    private IImageService imageService;

    @Override
    public ApiImportImageReplyMsg handle(ApiImportImageMsg msg) throws GCloudException {

        CreateImageParams params = BeanUtil.copyProperties(msg, CreateImageParams.class);
        String imageId = imageService.createImage(params, msg.getCurrentUser());
        ApiImportImageReplyMsg reply = new ApiImportImageReplyMsg();
        reply.setImageId(imageId);
        msg.setObjectId(imageId);
		msg.setObjectName(CacheContainer.getInstance().getString(CacheType.IMAGE_NAME, imageId));
        return reply;
    }
}