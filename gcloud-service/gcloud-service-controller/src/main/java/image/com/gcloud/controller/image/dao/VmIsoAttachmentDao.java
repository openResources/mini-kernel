package com.gcloud.controller.image.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gcloud.controller.image.entity.VmIsoAttachment;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Repository
public class VmIsoAttachmentDao  extends JdbcBaseDaoImpl<VmIsoAttachment, Long>{
	public List<VmIsoAttachment> findByIsoIdAndInstanceId(String isoId, String instanceId) {
		String sql = "select * from gc_vm_iso_attachments where iso_id = ? and instance_id = ?";

		List<Object> valueList = new ArrayList<Object>();
		valueList.add(isoId);
		valueList.add(instanceId);
		return this.findBySql(sql, valueList);
	}
}