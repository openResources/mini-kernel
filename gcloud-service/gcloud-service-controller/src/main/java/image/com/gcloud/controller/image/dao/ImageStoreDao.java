package com.gcloud.controller.image.dao;

import org.springframework.stereotype.Repository;

import com.gcloud.controller.image.entity.ImageStore;
import com.gcloud.framework.db.dao.impl.JdbcBaseDaoImpl;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Repository
public class ImageStoreDao extends JdbcBaseDaoImpl<ImageStore, String>{
	public int deleteByImageId(String imageId){

        String sql = "delete from gc_image_stores where image_id = ?";
        Object[] values = {imageId};
        return this.jdbcTemplate.update(sql, values);
    }
}