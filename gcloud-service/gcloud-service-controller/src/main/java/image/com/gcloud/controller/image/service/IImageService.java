package com.gcloud.controller.image.service;

import com.gcloud.controller.image.entity.Image;
import com.gcloud.controller.image.model.CreateImageParams;
import com.gcloud.controller.image.model.DescribeImageParams;
import com.gcloud.controller.image.model.DetailImageParams;
import com.gcloud.controller.image.model.UploadImageParams;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.framework.db.PageResult;
import com.gcloud.header.api.model.CurrentUser;
import com.gcloud.header.image.model.DetailImage;
import com.gcloud.header.image.model.ImageStatisticsItem;
import com.gcloud.header.image.model.ImageType;
import com.gcloud.header.image.msg.api.GenDownloadVo;
import org.springframework.core.io.Resource;

import java.util.List;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public interface IImageService {

    String createImage(CreateImageParams params, CurrentUser currentUser) throws GCloudException;

    void updateImage(String imageId, String imageName) throws GCloudException;

    void deleteImage(String imageId) throws GCloudException;

    PageResult<ImageType> describeImage(DescribeImageParams params, CurrentUser currentUser);

    Map<String, String> getImageProperties(String imageId);

    GenDownloadVo genDownload(String imageId);
    
    void disableImage(String imageId, Boolean disable);
    
    List<ImageStatisticsItem> imageStatistics();
    
    DetailImage detailImage(DetailImageParams params, CurrentUser currentUser);
    
    Image getById(String imageId);

    Resource download(String imageId);

    String upload(UploadImageParams params);

}