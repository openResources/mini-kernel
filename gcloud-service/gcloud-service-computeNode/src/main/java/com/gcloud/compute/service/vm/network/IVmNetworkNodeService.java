package com.gcloud.compute.service.vm.network;

import com.gcloud.header.compute.msg.node.vm.model.VmNetworkDetail;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public interface IVmNetworkNodeService {

    void detachPort(String instanceId, VmNetworkDetail networkDetail);
    void cleanNetEnvConfig(String portId, VmNetworkDetail networkDetail);
    void forceCleanNetEnvConfig(String portId, VmNetworkDetail networkDetail);
    void cleanNetConfigFile(String instanceId, VmNetworkDetail networkDetail);
    void forceCleanNetConfigFile(String instanceId, VmNetworkDetail networkDetail);
    void forceDetach(String instanceId, String portId, String macAddress);

    void configNetEnv(String instanceUuid, VmNetworkDetail vmNetworkDetail);
    void configNetFile(String instanceUuid, VmNetworkDetail vmNetworkDetail);
    void attachPort(String instanceId, VmNetworkDetail vmNetworkDetail);



}