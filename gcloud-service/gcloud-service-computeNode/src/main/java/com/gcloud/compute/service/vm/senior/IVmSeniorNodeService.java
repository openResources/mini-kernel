package com.gcloud.compute.service.vm.senior;

import com.gcloud.header.compute.enums.FileFormat;
import com.gcloud.header.storage.model.VmVolumeDetail;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public interface IVmSeniorNodeService {

    void convertToImage(String instanceId, FileFormat targetFormat, VmVolumeDetail vmVolumeDetail);

    void deleteBundle(String instanceId, String nodeIp);
    
    void migrate(String type, String vmMigrateType, String instanceId, String targetIp, String targetHostName, Boolean isStorageAll);

    void migrateInstance(String type, String migrateProtocol, String instanceId, String sourceIp, String targetIp, String targetHostName, Boolean isStorageAll);
    
}