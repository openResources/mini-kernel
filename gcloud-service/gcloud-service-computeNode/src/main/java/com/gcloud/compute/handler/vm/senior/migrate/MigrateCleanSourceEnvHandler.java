package com.gcloud.compute.handler.vm.senior.migrate;

import com.gcloud.compute.service.vm.trash.IVmTrashNodeService;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.msg.node.vm.senior.migrate.MigrateCleanSourceEnvMsg;
import com.gcloud.header.compute.msg.node.vm.senior.migrate.MigrateCleanSourceEnvReplyMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Slf4j
@Handler
public class MigrateCleanSourceEnvHandler extends AsyncMessageHandler<MigrateCleanSourceEnvMsg>{

	@Autowired
    private MessageBus bus;
	
	@Autowired
	private IVmTrashNodeService transhNodeService;
	
	@Override
	public void handle(MigrateCleanSourceEnvMsg msg) {
		MigrateCleanSourceEnvReplyMsg replyMsg = msg.deriveMsg(MigrateCleanSourceEnvReplyMsg.class);
		replyMsg.setSuccess(true);
		//清理源节点的虚拟机信�?
		try{
			transhNodeService.cleanInstanceInfo(msg.getInstanceId());
		} catch(Exception e) {
			log.info("清理虚拟机["+ msg.getInstanceId() +"]失败");
		}
		
		replyMsg.setServiceId(MessageUtil.controllerServiceId());
		bus.send(replyMsg);
	}

}