package com.gcloud.compute.handler.vm.create;

import com.gcloud.compute.service.vm.create.IVmCreateNodeService;
import com.gcloud.core.exception.GCloudException;
import com.gcloud.core.handle.AsyncMessageHandler;
import com.gcloud.core.handle.Handler;
import com.gcloud.core.messagebus.MessageBus;
import com.gcloud.core.util.MessageUtil;
import com.gcloud.header.compute.msg.node.vm.create.CreateDiskFileMsg;
import com.gcloud.header.compute.msg.node.vm.create.CreateDiskFileReplyMsg;
import org.springframework.beans.factory.annotation.Autowired;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Handler
public class CreateDiskFileHandler extends AsyncMessageHandler<CreateDiskFileMsg> {

	@Autowired
	private MessageBus bus;

	@Autowired
	private IVmCreateNodeService vmCreateNodeService;
	
	@Override
	public void handle(CreateDiskFileMsg msg) throws GCloudException {
		CreateDiskFileReplyMsg replyMsg = new CreateDiskFileReplyMsg();
		replyMsg.setTaskId(msg.getTaskId());
		replyMsg.setServiceId(MessageUtil.controllerServiceId());
		replyMsg.setSuccess(true);
		try {
			// TODO vmCreateNodeService.createDiskFile(msg.getStorageType(), msg.getImageId(), msg.getVolumeId(), msg.getSystemSize());
		} catch (Exception e) {
			replyMsg.setErrorCode(e.getMessage());
			replyMsg.setSuccess(false);
		}
		bus.send(replyMsg);
	}

}