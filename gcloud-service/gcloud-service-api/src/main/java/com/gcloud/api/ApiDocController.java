package com.gcloud.api;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.swing.plaf.basic.BasicScrollPaneUI.VSBChangeListener;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gcloud.core.util.ApiDocUtil;
import com.gcloud.header.ApiVersion;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@RestController
@Slf4j
public class ApiDocController {
	@GetMapping("/apidoc")
	public void apiDoc(HttpServletResponse response){

		String fileName = "gcloud8_api.md";
		String markdownStr = ApiDocUtil.getInstance().getDoc();

		response.addHeader("Content-Disposition", "attachment;filename=" + fileName);
		response.addHeader("Content-Type", "text/markdown; charset=UTF-8");

		try(OutputStream out = response.getOutputStream()){
			out.write(markdownStr.getBytes(StandardCharsets.UTF_8));
		}catch (Exception ex){
			log.error("write response error:" + ex, ex);
		}

	}
	
	@GetMapping("/apiMSDoc")
	public void apiMsDoc(@RequestParam("versions") List<String> versions, HttpServletResponse response) {
		String fileName = "gcloud8_api.docx";
		
		List<ApiVersion> versionList = new ArrayList<>();
		if(versions == null || versions.isEmpty()) {
			versionList.add(ApiVersion.V1);
		} else {
			for (String version : versions) {
				ApiVersion item = ApiVersion.valueOf(version);
				if(null != item) {
					versionList.add(item);
				}
			}
		}
		
		ApiVersion[] reqVersion = new ApiVersion[versionList.size()];
		reqVersion = versionList.toArray(reqVersion);
		ApiDocUtil.getInstance().createMsDoc(fileName, reqVersion);
		
		// docx的content-type
		response.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
		response.addHeader("Content-Disposition", "attachment;filename=" + fileName);
		
		try(OutputStream out = response.getOutputStream();
			InputStream input = new FileInputStream(new File(fileName))){
			IOUtils.copy(input, out);
			out.flush();
		} catch(Exception e) {
			log.error("write response error:" + e, e);
		}
	}
}