package com.gcloud.common.crypto;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */
public class Crypto
{
	public static String generateQueryId()
	{
		return Crypto.getCryptoProvider().generateQueryId();
	}
	
	/*
	 *  generate id with resource prefix
	 *  using queryId as a seed
	 */
	public static String generateResourceId( final String prefix )
	{
		String seed = generateQueryId();
		return generateId( seed, prefix ).toLowerCase();
	}
	
	public static String generateInstanceId( final String prefix )
	{
		String seed = generateQueryId();
		String result = generateId( seed, prefix ).toUpperCase();
		return result.replaceFirst(prefix.toUpperCase(), prefix);
	}
	
	public static String generateMacAddress()
	{
		String prefix = "";
		String seed = generateQueryId();
		String tmp = generateId( seed, prefix );
		String res = tmp.substring(1, tmp.length());
		seed = generateQueryId();
		tmp = generateId( seed, prefix );
		res += tmp.substring(1, tmp.length() - 4);
		res = res.toLowerCase();
		String result = "";
		for( int i = 0; i < res.length(); i += 2 ) {
			if( i != 0 ) {
				result += ":";
			}
			if( i == 0 ) {
				result += "d0";
			}
			else if( i == 1 ) {
				result += "0d";
			}
			else {
				result += res.substring(i, i+2);
			}
		}
		return result;
	}
	
	public static String generateId( final String seed, final String prefix )
	{
		return Crypto.getCryptoProvider( ).generateId( seed, prefix );
	}
	
	public static CryptoProvider getCryptoProvider( )
	{
//		return (CryptoProvider) providers.get( CryptoProvider.class );
		return new DefaultCryptoProvider();
	}
  
	public static void main(String[] args){
		System.out.println(generateResourceId("t"));
	}
	
	public static String generateResourceIdUpper(final String prefix) {

		String seed = generateQueryId();
		String result = generateId(seed, prefix).toUpperCase();
		return result.replaceFirst(prefix.toUpperCase(), prefix);

	}
}