/*
 * @Date 2015-12-16
 * 
 * @Author dengyf@g-cloud.com.cn
 * 
 * @Copyright 2015 www.g-cloud.com.cn Inc. All rights reserved. 
 * 
 * @Description 公共线程�?
 */
package com.gcloud.common.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



public class CommonThreadPool {
	private static final int POOL_SIZE = 30;
	private static ExecutorService fixedThreadPool = Executors
			.newFixedThreadPool(POOL_SIZE);

	public static ExecutorService getThreadPool() {
		return fixedThreadPool;
	}
}