package com.gcloud.core.util;

import java.util.List;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class SqlUtil {
	public static String listToInStr(List<String> values) {
		String valueStr = "";
		for (String value : values) {
			valueStr += "'" + value + "',";
		}
		if (!"".equals(valueStr)) {
			valueStr = valueStr.substring(0, valueStr.length() - 1);
		}
		return valueStr;
	}
	
	/**
	 * @Title: inPreStr
	 * @Description: 返回指定数量的in (?, ?) 条件语句
	 * 
	 * @param num
	 * @return 形如"?, ?, ?"的字符串
	 */
	public static String inPreStr(int num) {

		String inStr = "";
		for (int i = 0; i < num; i++) {
			inStr += "?,";
		}

		if (!"".equals(inStr)) {
			inStr = inStr.substring(0, inStr.length() - 1);
		}

		return inStr;
	}
}