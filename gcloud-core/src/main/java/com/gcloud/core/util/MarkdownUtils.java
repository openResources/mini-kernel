package com.gcloud.core.util;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class MarkdownUtils {
	public static String tableTitle(String... titles){
		StringBuilder sb=new StringBuilder("\n|");
		for(String title:titles){
			sb.append(title).append("|");
		}
		sb.append("\n|");
		for(String title:titles){
			sb.append("------|");
		}
		sb.append("\n");
		return sb.toString();
	}
	public static String tableRow(String... contends){
		StringBuilder sb=new StringBuilder("|");
		for(String contend:contends){
			sb.append(contend).append("|");
		}
		sb.append(" \n ");
		return sb.toString();
	}
	
	public static String title(int level,String title){
		StringBuilder sb=new StringBuilder("\n");
		for(int i=0;i<level;i++){
			sb.append("#");
		}
		sb.append(" ").append(title);
		sb.append("\n");
		return sb.toString();
	}
}