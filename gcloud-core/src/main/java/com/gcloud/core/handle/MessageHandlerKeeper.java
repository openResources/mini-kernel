package com.gcloud.core.handle;

import java.util.HashMap;
import java.util.Map;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class MessageHandlerKeeper {
	private static Map<String,MessageHandler> handlers=new HashMap<>();
	private static Map<String,AsyncMessageHandler> asyncHandlers=new HashMap<>();
	private static Map<String, MessageTimeoutHandler> timeoutHandlers=new HashMap<>();
	public static void put(String name,MessageHandler handler){
		handlers.put(name, handler);
	}
	public static MessageHandler get(String name){
		return handlers.get(name);
	}
	public static void put(String name,AsyncMessageHandler handler){
		asyncHandlers.put(name, handler);
	}
	public static AsyncMessageHandler getAsyncHandler(String name){
		return asyncHandlers.get(name);
	}
	public static MessageTimeoutHandler getTimeoutHandler(String name){
		return timeoutHandlers.get(name);
	}
	public static void put(String name, MessageTimeoutHandler handler){
		timeoutHandlers.put(name, handler);
	}
}