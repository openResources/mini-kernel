package com.gcloud.core.workflow.mng.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gcloud.core.workflow.dao.IWorkFlowInstanceDao;
import com.gcloud.core.workflow.entity.WorkFlowInstance;
import com.gcloud.core.workflow.mng.IWorkFlowInstanceMng;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */

@Service
@Transactional
public class WorkFlowInstanceMng implements IWorkFlowInstanceMng{
	@Autowired
	private IWorkFlowInstanceDao workFlowInstanceDao;

	@Override
	public void update(WorkFlowInstance ins, List<String> fields) {
		workFlowInstanceDao.update(ins, fields);
	}

	@Override
	public Long save(WorkFlowInstance ins) {
		return workFlowInstanceDao.saveWithIncrementId(ins);
	}

	@Override
	public WorkFlowInstance findById(Long id) {
		return workFlowInstanceDao.getById(id);
	}

	@Override
	public WorkFlowInstance findUnique(String field, String value) {
		return workFlowInstanceDao.findUniqueByProperty(field, value);
	}

	@Override
	public WorkFlowInstance getSubFlow(Long flowId, int stepId) {
		Map<String,Object> search = new HashMap<String,Object>();
		search.put("parentFlowId", flowId);
		search.put("parentFlowStepId", stepId);
		return workFlowInstanceDao.findUniqueByProperties(search);
	}

}