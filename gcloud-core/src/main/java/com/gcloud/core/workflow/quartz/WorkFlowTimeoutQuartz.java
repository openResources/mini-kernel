package com.gcloud.core.workflow.quartz;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

import com.gcloud.core.quartz.GcloudJobBean;
import com.gcloud.core.quartz.annotation.QuartzTimer;
import com.gcloud.core.service.SpringUtil;
import com.gcloud.core.workflow.core.BaseWorkFlowCommand;
import com.gcloud.core.workflow.entity.WorkFlowInstanceStep;
import com.gcloud.core.workflow.mng.IWorkFlowInstanceStepMng;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



@Component
@QuartzTimer(fixedRate = 60 * 1000L)
public class WorkFlowTimeoutQuartz extends GcloudJobBean{

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		IWorkFlowInstanceStepMng mng = (IWorkFlowInstanceStepMng)SpringUtil.getBean("workFlowInstanceStepMng");
	    List<WorkFlowInstanceStep> steps = mng.getAllNotFinishedStep();
	    for(WorkFlowInstanceStep step:steps)
	    {
	    	if(step.getTimeOut()!= 0 && addSecOfDate(step.getStartTime(), step.getTimeOut()).before(new Date())) {
		    	BaseWorkFlowCommand command = (BaseWorkFlowCommand)SpringUtil.getBean(step.getExecCommand());
				command.setWorkFlowStepId(step.getId());
				command.timeoutHandler();
	    	}
	    }
	}
	
	private Date addSecOfDate(Date date,int i)
	{
		Calendar c = Calendar.getInstance();
	    c.setTime(date);
	    c.add(Calendar.SECOND, i);
	    Date newDate = c.getTime();
	    return newDate;
	}

}