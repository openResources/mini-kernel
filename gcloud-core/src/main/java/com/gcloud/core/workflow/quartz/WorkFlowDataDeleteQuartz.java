package com.gcloud.core.workflow.quartz;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.gcloud.core.quartz.GcloudJobBean;
import com.gcloud.core.quartz.annotation.QuartzTimer;
import com.gcloud.core.workflow.mng.IFlowTaskMng;

import lombok.extern.slf4j.Slf4j;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */



@Slf4j
@Component
@QuartzTimer(fixedRate = 6 * 60 * 1000L)
public class WorkFlowDataDeleteQuartz extends GcloudJobBean {
	 @Value("${gcloud.controller.workflow.expireDay:30}")
	 private Integer expireDay;
	 
	@Autowired
	IFlowTaskMng flowTaskMng;

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		log.info("WorkFlowDataDeleteQuartz start");
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
			flowTaskMng.deleteWorkflowExpireData(sdf.format(DateUtils.round(DateUtils.addDays(new Date(), -expireDay), Calendar.DATE)));
		}catch(Exception ex) {
			log.error("WorkFlowDataDeleteQuartz error," + ex.getMessage());
		}
		log.info("WorkFlowDataDeleteQuartz end");
	}

}