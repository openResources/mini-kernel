package com.gcloud.core.workflow.mng;

import java.util.Date;
import java.util.List;

import com.gcloud.core.workflow.entity.BatchWorkFlow;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface IBatchWorkFlowMng {
	void update(BatchWorkFlow batchFlow, List<String> fields);
	void delete(BatchWorkFlow batchFlow);
	Long save(BatchWorkFlow batchFlow);
	BatchWorkFlow findById(Long id);
	BatchWorkFlow findUnique(String field,String value);
	List<BatchWorkFlow> findByProperty(String field,String value);
}