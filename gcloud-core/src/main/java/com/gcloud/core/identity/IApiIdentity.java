package com.gcloud.core.identity;

import com.gcloud.header.identity.api.GetApiReplyMsg;
import com.gcloud.header.identity.ldap.GetLdapConfReplyMsg;
//import com.gcloud.header.identity.role.ApiGetFunctionRightReplyMsg;
import com.gcloud.header.identity.role.DescribeUncheckApiReplyMsg;
import com.gcloud.header.identity.role.model.CheckRightReplyMsg;
import com.gcloud.header.identity.user.GetUserReplyMsg;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public interface IApiIdentity {
	TokenUser checkToken(String token);
	CheckRightReplyMsg checkRight(String roleId, String regionId, String funcPath);
	SignUser getUserByAccessKey(String accessKey);
	GetUserReplyMsg getUserById(String userId);
//	ApiGetRoleRightReplyMsg getRoleRight(String roleId, String regionId);
//	ApiDetailRoleReplyMsg getRoleDetail(String roleId);
//	ApiGetFunctionRightReplyMsg getFunctionRight(String path);
	GetApiReplyMsg getApi(String path);
	DescribeUncheckApiReplyMsg getUncheckApi(String type);
	GetLdapConfReplyMsg getLdapConf(String domain);
}