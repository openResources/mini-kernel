package com.gcloud.core.quartz.model;

import java.io.Serializable;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


public class QuartzJobFixedData implements Serializable {
	private static final long serialVersionUID = 1L;
	
//	private JobFixedType jobFixedType;
	private String jobFixedType;
    private long interval;

    public QuartzJobFixedData(String jobFixedType, long interval) {
        this.jobFixedType = jobFixedType;
        this.interval = interval;
    }

    public QuartzJobFixedData() {
    }


    public String getJobFixedType() {
		return jobFixedType;
	}

	public void setJobFixedType(String jobFixedType) {
		this.jobFixedType = jobFixedType;
	}

	public long getInterval() {
        return interval;
    }

    public void setInterval(long interval) {
        this.interval = interval;
    }
}