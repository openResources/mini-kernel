package com.gcloud.core.currentUser.policy.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.gcloud.core.currentUser.enums.RoleType;
import com.gcloud.core.currentUser.policy.enums.ResourceIsolationCheckType;
import com.gcloud.core.currentUser.policy.model.FilterPolicyModel;
import com.gcloud.header.api.model.CurrentUser;
/**
Copyright (c) [2020] [G-CLOUD TECHNOLOGY] [G-Cloud 8.0] is licensed under the Mulan PSL v1.
You can use this software according to the terms and conditions of the Mulan PSL v1. You may obtain
a copy of Mulan PSL v1 at: http://license.coscl.org.cn/MulanPSL THIS SOFTWARE IS PROVIDED ON AN 
"AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE. See the Mulan PSL v1 for more details.
 */


@Service
public class TypicalUserResourceFilterPolicyImpl implements IUserResourceFilterPolicy {

	@Override
	public FilterPolicyModel filterPolicy(CurrentUser currentUser, String prefix,
			ResourceIsolationCheckType checkType) {
		FilterPolicyModel model = new FilterPolicyModel();
		List<Object> params = new ArrayList<Object>();
		StringBuffer sqlWhereBuff = new StringBuffer();
		sqlWhereBuff.append(" AND ");

		// 超级管理�?
		if (currentUser.getRole().equals(RoleType.SUPER_ADMIN.getRoleId())) {
			if(!StringUtils.isBlank(currentUser.getDefaultTenant())) {
				sqlWhereBuff.append( prefix + "tenant_id = ? ");
				params.add(currentUser.getDefaultTenant());
			} else {
				sqlWhereBuff.append(" 1 = 1 ");
			}
			model.setParams(params);
			model.setWhereSql(sqlWhereBuff.toString());
			return model;
		} else {
			return IsolationTypeChecks.get(checkType.getCheck().getIsolationType().getValue()).resourceFilter(currentUser, prefix);
		}
	}
	
}